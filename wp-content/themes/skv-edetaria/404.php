<?php get_header(); ?>


    <section class="billboard halfheight">
        <div class="noslider">
            <div class="overlay"></div>
            <div class="single-img bg-img-edetaria"></div>
        </div> <!-- /.noslider -->
    </section><!-- /.billboard  -->
    
    
    <main class="halfmargin">
        
        <section class="separator-header"></section>
        
        
        <section class="intro wrapper wrapper-margin">
            
            <h1><?php _e( 'Page not found', 'html5blank' ); ?></h1>
           
            <h2>
				<a href="<?php echo home_url(); ?>"><?php _e( 'Return home?', 'html5blank' ); ?></a>
            </h2>
            
        </section><!--  End Features  -->
        
        
        <section class="page-wrapper separator"></section>

    </main>


<?php get_footer(); ?>
