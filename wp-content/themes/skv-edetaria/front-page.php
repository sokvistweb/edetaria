<?php get_header(); ?>


    <section class="billboard fullheight index2">
        <div class="title-block">
            <div class="title-bg">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 500 61"><path d="M14 28.1c0-1.8 0-5.9-.6-7.4-.7-1.9-3-1.9-4.8-1.9v-1.5h27.7v8.4H35c-1.1-6.5-4-6.9-9.9-6.9h-4c-.8 0-1.2-.1-1.4.2-.1.2-.1.8-.1 2.1v13.2c7.6.3 8.8-2.8 9.1-6.9H30c-.1 2.5-.1 5.1-.2 7.6.1 2.3.2 4.6.2 6.9h-1.3c-.8-5.9-3.7-6.1-9.1-6.1V45c0 1.7 0 4.3 1 5.6 1.4 2 4.9 2.1 7.1 2 5.8-.1 9.6-.8 10.3-7.5h1.3l-.5 9H8.7v-1.3c1.8 0 4.1 0 4.8-1.9.6-1.5.6-5.6.6-7.4V28.1zM79.3 28c0-1.8 0-5.9-.5-7.4-.6-2-3-1.9-4.7-1.9v-1.4c2.3.1 4.7.2 7 .2 3-.1 6-.2 9-.2 11.8-.2 20.7 7.1 20.7 19.2 0 11.8-7.7 18-19.2 17.7-3.3-.1-6.6-.2-9.9-.2-2.6.1-5.1.2-7.6.2v-1.3c1.8 0 4.1 0 4.7-1.9.5-1.5.5-5.6.5-7.4V28zM85 44.4c0 1.9-.2 4.6.9 6.2 1.3 1.9 4.1 2.1 6.2 2.1 9.8 0 11.7-7 11.7-15.3 0-11-3.2-18.6-15.1-18.6-.7 0-2.8-.1-3.3.4-.4.4-.4 1.3-.4 1.7v23.5zM151.2 28.1c0-1.8 0-5.9-.6-7.4-.7-1.9-3-1.9-4.8-1.9v-1.5h27.7v8.4h-1.3c-1.1-6.5-4-6.9-9.9-6.9h-4c-.8 0-1.2-.1-1.4.2-.1.2-.1.8-.1 2.1v13.2c7.6.3 8.8-2.8 9.1-6.9h1.3c-.1 2.5-.1 5.1-.2 7.6.1 2.3.2 4.6.2 6.9H166c-.8-5.9-3.7-6.1-9.1-6.1V45c0 1.7 0 4.3 1 5.6 1.5 2 4.9 2.1 7.1 2 5.8-.1 9.6-.8 10.3-7.5h1.3l-.5 9h-30.3v-1.3c1.8 0 4.1 0 4.8-1.9.6-1.5.6-5.6.6-7.4V28.1zM212.6 26.9v-1.2c3.4-1.9 6.7-4.6 8.1-8.4h1v7.8h7.6v1.8h-7.6V46c0 3 .6 6.2 4.2 6.2 2.3 0 3.6-1.3 4.8-3l1.2.9c-1.2 3.2-4.6 4.3-7.8 4.3-4.8 0-8.1-1.9-8.1-6.9V26.9h-3.4zM276.7 42.1c-.8 2.1-2.7 6.2-2.7 8.4 0 2.4 2.8 2.2 4.5 2.4v1.3c-2.1-.1-4.1-.2-6.1-.2-2 .1-3.8.2-5.8.2v-1.3c3-.3 4.2-1.2 5.4-4 1.6-3.6 3.1-7.1 4.6-10.6l8.5-21.9h1.9l11.3 28.3c.7 1.4 1.9 4.9 2.9 6.1 1.2 1.6 2.9 1.8 4.8 2.1v1.3c-2.6-.1-5.3-.2-7.9-.2-2.7.1-5.4.2-8.1.2v-1.3c1.6-.1 4.3.1 4.3-2.1 0-1.4-.6-3.1-1.1-4.3l-2.2-5.7h-13.8l-.5 1.3zm13.6-3.1l-6.1-15.7L278 39h12.3zm.7-27.2l-7.5-3.2c-1.3-.6-3.5-1.2-3.5-3 0-1.1.8-2 1.9-2 .9 0 1.8.7 2.5 1.3l8.8 6.9H291zM368.1 49.7c1.8 2.1 2.6 2.8 5.5 3.3v1.3c-1.7-.1-3.3-.2-5-.2-1.4.1-2.8.2-4.2.2l-2.6-3.2-9.3-11.5c-1.8-2.1-1.6-2.5-4.6-2.5v6.6c0 1.8 0 5.9.5 7.4.7 2 3 1.9 4.7 1.9v1.3c-2.6-.1-5.2-.2-7.9-.2-2.8.1-5.6.2-8.4.2V53c1.8 0 4.1 0 4.7-1.9.5-1.5.5-5.6.5-7.4V27.9c0-1.8 0-5.9-.5-7.4-.6-2-3-1.9-4.7-1.9v-1.3c3.2.1 6.4.2 9.6.2 2.3-.1 4.7-.2 7.1-.2 6.4 0 12.8 1.4 12.8 9.2 0 6-4.6 9.1-10.1 9.6l11.9 13.6zm-20.2-14.1c7 0 12-.2 12-8.3 0-4.9-2.8-8.4-7.8-8.4-.7 0-3.1-.2-3.6.1-.7.3-.6 1.4-.6 2v14.6zM410.1 27.9c0-1.8 0-5.9-.6-7.4-.7-1.9-3-1.9-4.8-1.9v-1.3c2.7.1 5.3.2 8 .2 2.8-.1 5.6-.2 8.4-.2v1.3c-1.8 0-4.1 0-4.8 1.9-.6 1.5-.6 5.6-.6 7.4v15.7c0 1.8 0 5.9.6 7.4.7 1.9 3 1.9 4.8 1.9v1.3c-2.6-.1-5.4-.2-8-.2-2.8.1-5.6.2-8.4.2v-1.3c1.8 0 4.1 0 4.8-1.9.6-1.5.6-5.6.6-7.4V27.9zM462.4 42.1c-.8 2.1-2.7 6.2-2.7 8.4 0 2.4 2.8 2.2 4.5 2.4v1.3c-2.1-.1-4.1-.2-6.1-.2-2 .1-3.8.2-5.8.2v-1.3c3-.3 4.2-1.2 5.4-4 1.6-3.6 3.1-7.1 4.6-10.6l8.5-21.9h1.9L484 44.8c.7 1.4 1.9 4.9 2.9 6.1 1.2 1.6 2.9 1.8 4.8 2.1v1.3c-2.6-.1-5.3-.2-7.9-.2-2.7.1-5.4.2-8.1.2V53c1.6-.1 4.3.1 4.3-2.1 0-1.4-.6-3.1-1.1-4.3l-2.2-5.7h-13.8l-.5 1.2zM476 39l-6.1-15.7-6.3 15.7H476z"/></svg>
                <h1 class="big-title"><?php the_excerpt() ?></h1>
                <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                <h2 class="title-desc"><?php the_content(); ?></h2>
                <?php endwhile; endif; wp_reset_postdata(); ?>
            </div>
        </div>
        <div class="slider">
            <div class="flexslider">
                <ul class="slides">
                    <li>
                        <div class="overlay"></div>
                        <div class="slider-caption visible-md visible-lg"></div>
                    </li>
                    <li>
                        <div class="overlay"></div>
                        <div class="slider-caption visible-md visible-lg"></div>
                    </li>
                    <li>
                        <div class="overlay"></div>
                        <div class="slider-caption visible-md visible-lg"></div>
                    </li>
                </ul>
            </div> <!-- /.flexslider -->
        </div> <!-- /.slider -->
    </section><!-- /.billboard  -->
    
    
    <main class="fullmargin">
       
       <section class="page-wrapper">
            <div class="spotlight grid">
                <?php query_posts('post_type=page&name=edetaria'); while (have_posts ()): the_post(); ?>
                <figure class="effect-skv image">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/som-01.jpg" alt="Edetària" width="900" height="520" />
                    <figcaption>
                        <div class="fig-content">
                            <h3><?php the_title() ?></h3>
                            <h2><?php the_excerpt() ?></h2>
                            <div class="fig-subcontent">
                                <p class="to-anchor"><a href="<?php the_permalink(); ?>#qui-som"><?php the_field('titol_del_link_1'); ?></a></p>
                                <p class="to-anchor"><a href="<?php the_permalink(); ?>#un-estil-propi"><?php the_field('titol_del_link_2'); ?></a></p>
                                <p class="to-anchor"><a href="<?php the_permalink(); ?>#territori"><?php the_field('titol_del_link_3'); ?></a></p>
                                <p class="to-anchor"><a href="<?php the_permalink(); ?>#d-on-ve-edetaria"><?php the_field('titol_del_link_4'); ?></a></p>
                                <p class="to-anchor"><a href="<?php the_permalink(); ?>#equip"><?php the_field('titol_del_link_5'); ?></a></p>
                                <div class="separator-hover2"></div>
                            </div>
                        </div>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title() ?> - <?php the_excerpt() ?>">
                            <?php if(function_exists('qtranxf_getLanguage')) { ?>
                            <?php if (qtranxf_getLanguage()=='ca'): ?>
                            Veure més
                            <?php endif; ?>
                            <?php if (qtranxf_getLanguage()=='es'): ?>
                            Ver más
                            <?php endif; ?>
                            <?php if (qtranxf_getLanguage()=='en'): ?>
                            Read more
                            <?php endif; ?>
                            <?php } ?>
                        </a>
                    </figcaption>			
                </figure>
                <?php endwhile; wp_reset_postdata(); ?>
                <?php query_posts('post_type=page&name=vinyes-i-terra'); while (have_posts ()): the_post(); ?>
                <figure class="effect-skv color1 image">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/vinyes-i-terra-01.jpg" alt="Edetària" width="900" height="520" />
                    <figcaption>
                        <div class="fig-content">
                            <h3><?php the_title() ?></h3>
                            <h2><?php the_excerpt() ?></h2>
                            <div class="fig-subcontent">
                                <p class="to-anchor"><a href="<?php the_permalink(); ?>#els-sols"><?php the_field('titol_del_link_1'); ?></a></p>
                                <p class="to-anchor"><a href="<?php the_permalink(); ?>#les-varietats-de-raim"><?php the_field('titol_del_link_2'); ?></a></p>
                                <p class="to-anchor"><a href="<?php the_permalink(); ?>#conreu-ecologic"><?php the_field('titol_del_link_3'); ?></a></p>
                                <p class="to-anchor"><a href="<?php the_permalink(); ?>#projectes-experimentals"><?php the_field('titol_del_link_4'); ?></a></p>
                                <div class="separator-hover6"></div>
                            </div>
                        </div>
				        <a href="<?php the_permalink(); ?>" title="<?php the_title() ?> - <?php the_excerpt() ?>">
                            <?php if(function_exists('qtranxf_getLanguage')) { ?>
                            <?php if (qtranxf_getLanguage()=='ca'): ?>
                            Veure més
                            <?php endif; ?>
                            <?php if (qtranxf_getLanguage()=='es'): ?>
                            Ver más
                            <?php endif; ?>
                            <?php if (qtranxf_getLanguage()=='en'): ?>
                            Read more
                            <?php endif; ?>
                            <?php } ?>
                        </a>
                    </figcaption>
                </figure>
                <?php endwhile; wp_reset_postdata(); ?>
            </div><!-- /.spotlight -->
        </section>
            
            
        <section class="page-wrapper">
            <div class="spotlight grid">
                
                <?php query_posts('post_type=page&name=el-celler'); while (have_posts ()): the_post(); ?>
                    <figure class="effect-skv color1 image">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/el-celler.jpg" alt="Edetària" width="900" height="520" />
                        <figcaption>
                            <div class="fig-content">
                                <h3><?php the_title() ?></h3>
                                <h2><?php the_excerpt() ?></h2>
                                <div class="fig-subcontent">
                                    <p class="to-anchor"><a href="<?php the_permalink(); ?>#la-verema"><?php the_field('titol_link_2'); ?></a></p>
                                    <p class="to-anchor"><a href="<?php the_permalink(); ?>#vinificacions"><?php the_field('titol_link_3'); ?></a></p>
                                    <p class="to-anchor"><a href="<?php the_permalink(); ?>#criansa"><?php the_field('titol_link_1'); ?></a></p>
                                </div>
                            </div>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title() ?> - <?php the_excerpt() ?>">
                                <?php if(function_exists('qtranxf_getLanguage')) { ?>
                                <?php if (qtranxf_getLanguage()=='ca'): ?>
                                Veure més
                                <?php endif; ?>
                                <?php if (qtranxf_getLanguage()=='es'): ?>
                                Ver más
                                <?php endif; ?>
                                <?php if (qtranxf_getLanguage()=='en'): ?>
                                Read more
                                <?php endif; ?>
                                <?php } ?>
                            </a>
                        </figcaption>			
                    </figure>
                    <?php endwhile; wp_reset_postdata(); ?>
                
                    <?php query_posts('post_type=page&name=shop'); while (have_posts ()): the_post(); ?>
                    <figure class="effect-skv image">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/els-vins.jpg" alt="Edetària" width="900" height="520" />
                        <figcaption>
                            <div class="fig-content">
                                <?php if(function_exists('qtranxf_getLanguage')) { ?>
                                <?php if (qtranxf_getLanguage()=='ca'): ?>
                                <h3>Els vins</h3>
                                <?php endif; ?>
                                <?php if (qtranxf_getLanguage()=='es'): ?>
                                <h3>Los vinos</h3>
                                <?php endif; ?>
                                <?php if (qtranxf_getLanguage()=='en'): ?>
                                <h3>Our wines</h3>
                                <?php endif; ?>
                                <?php } ?>
                                <h2></h2>
                                <div class="fig-subcontent">
                                    <p class="to-anchor"><a href="/els-vins/via-terra/">Via <span>Terra</span></a></p>
                                    <p class="to-anchor"><a href="/els-vins/via-edetana/">Via <span>Edetana</span></a></p>
                                    <p class="to-anchor"><a href="/els-vins/edetaria-seleccio/">Edetària <span>Selecció</span></a></p>
                                    <p class="to-anchor"><a href="/els-vins/les-nostres-finques/">Les Nostres <span>Finques</span></a></p>
                                </div>
                            </div>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title() ?> - <?php the_excerpt() ?>">
                                <?php if(function_exists('qtranxf_getLanguage')) { ?>
                                <?php if (qtranxf_getLanguage()=='ca'): ?>
                                Comprar online
                                <?php endif; ?>
                                <?php if (qtranxf_getLanguage()=='es'): ?>
                                Comprar online
                                <?php endif; ?>
                                <?php if (qtranxf_getLanguage()=='en'): ?>
                                Buy online
                                <?php endif; ?>
                                <?php } ?>
                            </a>
                        </figcaption>
                    </figure>
                    <?php endwhile; wp_reset_postdata(); ?>
                
            </div>
        </section>
          
            
        <section class="page-wrapper">
            <div class="spotlight grid">
                <div class="container33">
                    <figure class="effect-skv3 image">
                        <img class="large-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/experiencies-enoturisme.jpg" alt="Edetària" width="900" height="460" />
                        <img class="small-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/experiencies-enoturisme-2.jpg" alt="Edetària" width="900" height="520" />
                        <figcaption>
                            <div class="fig-content">
                                <?php query_posts('post_type=page&name=enoturisme'); while (have_posts ()): the_post(); ?>
                                <h2><?php the_title() ?></h2>
                                <?php endwhile; wp_reset_postdata(); ?>
                                <ul>
                                    <?php query_posts('post_type=enoturisme&order=ASC'); while (have_posts ()): the_post(); ?>
                                    <li><a href="/experiencies/enoturisme#<?php global $post; $post_slug=$post->post_name; echo $post_slug; ?>"><?php the_title() ?></a></li>
                                    <?php endwhile; wp_reset_postdata(); ?>
                                </ul>
                            </div>
                            
                            <?php query_posts('post_type=page&name=enoturisme'); while (have_posts ()): the_post(); ?>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title() ?> - <?php the_excerpt() ?>">
                                <?php if(function_exists('qtranxf_getLanguage')) { ?>
                                <?php if (qtranxf_getLanguage()=='ca'): ?>
                                Veure més
                                <?php endif; ?>
                                <?php if (qtranxf_getLanguage()=='es'): ?>
                                Ver más
                                <?php endif; ?>
                                <?php if (qtranxf_getLanguage()=='en'): ?>
                                Read more
                                <?php endif; ?>
                                <?php } ?>
                            </a>
                            <?php endwhile; wp_reset_postdata(); ?>
                            
                        </figcaption>
                    </figure>
                </div>
                
                <div class="container33">
                    <?php query_posts('post_type=page&name=empreses'); while (have_posts ()): the_post(); ?>
                    <figure class="effect-skv3 image">
                        <img class="large-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/experiencies-empreses.jpg" alt="Edetària" width="900" height="460" />
                        <img class="small-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/experiencies-empreses-2.jpg" alt="Edetària" width="900" height="520" />
                        <figcaption>
                            <div class="fig-content">
                                <h2><?php the_title() ?></h2>
                                <p><em><?php the_excerpt() ?></em></p>
                            </div>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title() ?> - <?php the_excerpt() ?>">
                                <?php if(function_exists('qtranxf_getLanguage')) { ?>
                                <?php if (qtranxf_getLanguage()=='ca'): ?>
                                Veure més
                                <?php endif; ?>
                                <?php if (qtranxf_getLanguage()=='es'): ?>
                                Ver más
                                <?php endif; ?>
                                <?php if (qtranxf_getLanguage()=='en'): ?>
                                Read more
                                <?php endif; ?>
                                <?php } ?>
                            </a>
                        </figcaption>
                    </figure>
                    <?php endwhile; wp_reset_postdata(); ?>
                </div>
                
                <div class="container33">
                    <?php query_posts('post_type=page&name=espais'); while (have_posts ()): the_post(); ?>
                    <figure class="effect-skv3 image">
                        <img class="large-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/experiencies-espais.jpg" alt="Edetària" width="900" height="460" />
                        <img class="small-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/experiencies-espais-2.jpg" alt="Edetària" width="900" height="520" />
                        <figcaption>
                            <div class="fig-content">
                                <h2><?php the_title() ?></h2>
                                <p><em><?php the_excerpt() ?></em></p>
                            </div>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title() ?> - <?php the_excerpt() ?>">
                                <?php if(function_exists('qtranxf_getLanguage')) { ?>
                                <?php if (qtranxf_getLanguage()=='ca'): ?>
                                Veure més
                                <?php endif; ?>
                                <?php if (qtranxf_getLanguage()=='es'): ?>
                                Ver más
                                <?php endif; ?>
                                <?php if (qtranxf_getLanguage()=='en'): ?>
                                Read more
                                <?php endif; ?>
                                <?php } ?>
                            </a>
                        </figcaption>
                    </figure>
                    <?php endwhile; wp_reset_postdata(); ?>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        
        <section class="page-wrapper">
            <div class="spotlight grid">
                <div class="container-full">
                    <figure class="effect-skv image">
                        <img class="large-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/noticies-large.jpg" alt="Edetària" width="1800" height="600" />
                        <img class="small-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/noticies.jpg" alt="Edetària" width="900" height="520" />
                        <figcaption>
                            <div class="fig-content">
                                <?php query_posts('post_type=page&name=noticies'); while (have_posts ()): the_post(); ?>
                                <h3><?php the_title() ?></h3>
                                <h2></h2>
                                <?php endwhile; wp_reset_postdata(); ?>

                                <?php $the_query = new WP_Query( 'posts_per_page=4' ); ?>
                                <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

                                <p><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></p>

                               <?php endwhile; wp_reset_postdata(); ?>
                            </div>

                            <?php if(function_exists('qtranxf_getLanguage')) { ?>
                            <?php if (qtranxf_getLanguage()=='ca'): ?>
                            <a href="/noticies" title="Veure totes les notícies">Totes les notícies</a>
                            <?php endif; ?>
                            <?php if (qtranxf_getLanguage()=='es'): ?>
                            <a href="/es/noticies" title="Ver todas las noticias">Todas las noticias</a>
                            <?php endif; ?>
                            <?php if (qtranxf_getLanguage()=='en'): ?>
                            <a href="/en/noticies" title="See all news">All news</a>
                            <?php endif; ?>
                            <?php } ?>

                        </figcaption>
                    </figure>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        
        <section class="page-wrapper separator"></section>
        
    </main>


<?php get_footer(); ?>
