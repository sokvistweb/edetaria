<?php /* Template Name: Pàgina Press Room */ get_header(); ?>
    
   
    <main class="nomargin">
        
        <section class="separator-header"></section>

        <section class="intro wrapper wrapper-margin">
            
            <h1><?php the_title(); ?></h1>
            
        </section><!--  End Features  -->


        <section class="wrapper wrapper-margin20">
            <div class="spotlight">
                <div class="image entry-image">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/press-room/edetaria-logo-tot.jpg" alt="Edetària" width="800" height="530" />
                </div>
                
                <div class="container entry-container">
                    <article class="content post">
                        <div class="entry-header">
                            <h2><a href="#">Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s</a></h2>
                        </div>
                        <div class="entry-excerpt clearfix">
                            <p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ‘Content here, content here’, making it look like readable English.</p>
                            <div class="read-more cl-effect-14">
                                <a href="#" class="more-link">Descarregar <span class="meta-nav">↓</span></a>
                            </div>
                        </div>
                    </article>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        
        <section class="wrapper wrapper-margin20">
            <div class="spotlight">
                <div class="image entry-image">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/press-room/edetaria-logo.jpg" alt="Edetària" width="800" height="530" />
                </div>
                
                <div class="container entry-container">
                    <article class="content post">
                        <div class="entry-header">
                            <h2><a href="#">Lorem Ipsum as their default model text, and a search for ‘lorem ipsum’ will uncover many web sites still in their infancy</a></h2>
                        </div>
                        <div class="entry-excerpt clearfix">
                            <p>Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
                            <a href="#" class="more-link">Descarregar <span class="meta-nav">↓</span></a>
                        </div>
                    </article>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        
        <section class="wrapper wrapper-margin20">
            <div class="spotlight">
                <div class="image entry-image">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/press-room/pdf-doc.jpg" alt="Edetària" width="800" height="530" />
                </div>
                
                <div class="container entry-container">
                    <article class="content post">
                        <div class="entry-header">
                            <h2><a href="#">Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker</a></h2>
                        </div>
                        <div class="entry-excerpt clearfix">
                            <p> It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                            <a href="#" class="more-link">Descarregar <span class="meta-nav">↓</span></a>
                        </div>
                    </article>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        
        <section class="wrapper wrapper-margin20">
            <div class="spotlight">
                <div class="image entry-image">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/press-room/word-doc.jpg" alt="Edetària" width="800" height="530" />
                </div>
                
                <div class="container entry-container">
                    <article class="content post">
                        <div class="entry-header">
                            <h2><a href="#">Lorem Ipsum is simply dummy text of the printing and typesetting industry</a></h2>
                        </div>
                        <div class="entry-excerpt clearfix">
                            <p>Lorem Ipsum as their default model text, and a search for ‘lorem ipsum’ will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose.</p>
                            <div class="read-more cl-effect-14">
                                <a href="#" class="more-link">Descarregar <span class="meta-nav">↓</span></a>
                            </div>
                        </div>
                    </article>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="page-wrapper separator"></section>
        
    </main>


<?php get_footer(); ?>
