<?php include("header.php"); ?>

	
    <section class="billboard halfheight">
        <div class="noslider">
            <div class="overlay"></div>
            <div class="single-img bg-img-el_celler"></div>
        </div> <!-- /.noslider -->
    </section><!-- /.billboard  -->
    
    
    <main class="halfmargin">
        
        <section class="separator-header"></section>

        <section class="intro wrapper wrapper-margin">
            
            <h1>El celler</h1>
            <h2><em>Quan estimes un territori hi vius amb sintonia</em></h2>
            <p>El Celler es va construir l’any 2003 envoltat per una part de les vinyes que Edetària té en propietat. És un edifici singular de línies senzilles i modernes integrat en un paratge únic, envoltat de vinyes i muntanyes.  Tots els elements: vinyes, celler, la plana de Gandesa, les muntanyes de Pàndols-Cavalls i la serralada dels Ports, es fonen i formen part d’un escenari únic.</p>
            
        </section><!--  End Features  -->


        <section class="page-wrapper">
            <div class="spotlight">
                <div class="image">
                    <img src="assets/images/page-elceller-3.jpg" alt="Les habitacions" width="900" height="520" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <h2>Criança</h2>
                        <h3><em>Els vins d’Edetària, l’expressió d’una personalitat</em></h3>
                        <p>Durant el procés de criança, busquem preservar i potenciar el caràcter, els matisos aportats per les diferents vinyes i terrers.  El treball colze a colze amb els boters elegits ens permet cercar les botes que millor potenciïn l'expressió dels nostres vins, i finalment l'assemblatge de tots els matisos, per donar uns vins amb personalitat.</p>
                        <div class="separator-hover1"></div>
                    </div>
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight">
                <div class="container">
                    <div class="content">
                        <h2>La verema</h2>
                        <h3><em>Només els millors gotims</em></h3>
                        <p>La verema es fa manualment, única forma vàlida per a Edetària per seleccionar el millor raïm de cada cep.  Es diposita en caixes de petita dimensió, es refreda el raïm entre 0ºC a 5ºC i finalment fem una segona selecció en taula de tria. Per tant, només entren a les tines aquells gotims que disposen de l’adient maduració polifenòlica.</p>
                        <div class="separator-hover1"></div>
                    </div>
                </div>
                
                <div class="image">
                    <img src="assets/images/page-elceller-2.jpg" alt="Les habitacions" width="900" height="520" />
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight">
                <div class="image">
                    <img src="assets/images/page-elceller-4.jpg" alt="Les habitacions" width="900" height="520" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <h2>Vinificacions</h2>
                        <h3><em>L’expressió de cada parcel·la</em></h3>
                        <p>Les tines per l'elaboració són de petites dimensions. Estan pensades i equipades per vinificar les diferents parcel•les per separat i així seguir quina és l'evolució de cada intervenció que es fa des de la vinya fins a la copa.</p>
                        <div class="separator-hover1"></div>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="page-wrapper separator"></section>
        
    </main>


<?php include("footer.php"); ?>