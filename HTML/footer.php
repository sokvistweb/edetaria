	<footer>
        <div class="wrapper-footer">
            <div class="spotlight top-footer">
                <div class="footer-left">
                    <!-- Mailchimp Subscribe Form -->
                    <div id="mc_embed_signup" class="subscribe-form">
                        <h3>Subscriu-te al Club Edetària</h3>
                        <form action="#" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">

                            <div class="mc-field-group">
                                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                            </div>

                            <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Nom">
                            <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response"></div>
                                <div class="response" id="mce-success-response"></div>
                            </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div id="real-people" aria-hidden="true"><input type="text" name="b_e5b778581d428a50593d91c5f_e98b7563d8" tabindex="-1" value=""></div>
                            <div class="clear">
                                <input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="button">
                            </div>
                        </form>
                    </div> <!-- /.subscribe-form -->
                </div>

                <div class="footer-right">
                    <div class="contact-info">
                        <H3>Contacteu-nos i <a href="https://goo.gl/maps/n23d4Zwxei82" title="Veure a Google Maps" target="_blank">veniu a <span>Edetària</span></a>.</H3>
                        <address>FINCA EL MAS · Ctra. Gandesa - Vilalba, s/n · 43780 Gandesa (Tarragona)</address>
                        <a class="googlemap-small" href="https://goo.gl/maps/n23d4Zwxei82" title="Veure a Google maps" target="_blank">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 width="30px" height="30px" viewBox="156 -156 512 512" enable-background="new 156 -156 512 512" xml:space="preserve">
                            <g>
                                <path d="M412-124c-74,0-134.2,58.7-134.2,132.7c0,16.4,3.5,34.3,9.8,50.4h-0.1l0.6,1.2c0.5,1.1,1,2.2,1.5,3.3L412,324L533.8,64.9
                                    l0.6-1.2c0.5-1.1,1.1-2.2,1.6-3.4l0.4-1.1c6.5-16.1,9.8-33.1,9.8-50.3C546.2-65.3,486-124,412-124z M412,50.9
                                    c-25.9,0-46.9-21-46.9-46.9s21-46.9,46.9-46.9s46.9,21,46.9,46.9S437.9,50.9,412,50.9z"/>
                            </g>
                            </svg>
                            </a><br>
                        Telèfon / Fax <a href="tel:0034977421534">(+34) 977 42 15 34</a><br>
                        <a href="mailto:info@edetaria.com">info@edetaria.com</a> · <a href="http://www.edetaria.com" title="La web de Can Miquel">www.edetaria.com</a>
                    </div>

                    <ul class="social">
                        <li><a class="facebook" href="https://www.facebook.com/Edetaria/" title="La nostra pàgina de Facebook" target="_blank"></a></li>
                        <li><a class="twitter" href="https://twitter.com/EdetariaCeller" title="El nostre compte de Twitter" target="_blank"></a></li>
                        <li><a class="instagram" href="https://www.instagram.com/edetaria/" title="El nostre compte d'Instagram" target="_blank"></a></li>
                        <!--<li><a class="pinterest" href="#"></a></li>-->
                    </ul>
                </div>
            </div>
            
            
            <div class="spotlight bottom-footer">
                <div class="footer-left">
                    <ul class="footer-menu">
                        <li><a href="noticies.php">Clipping</a><span>Notícies i reculls de premsa</span></li>
                        <li><a href="#">Press Room</a><span>Continguts disponibles per a periodistes, editors, etc.</span></li>
                        <li><a href="contacte.php">Contacte</a><span>Veniu o contacteu-nos del del nostre formulari.</span></li>
                    </ul>
                </div>
                <div class="footer-right">
                    <ul class="legal-pages">
                        <li><a href="#">Transporte y devoluciones</a></li>
                        <li><a href="#">Forma de pago</a></li>
                        <li><a href="avis-legal.php">Aviso legal</a></li>
                        <li><a href="politica-de-cookies.php">Política de cookies</a></li>
                        <li><a href="politica-de-privacitat.php">Política de privacidad</a></li>
                    </ul>
                    <span class="rights">
                            © <?php echo date("Y"); ?> Edetària | <a href="#">Admin</a> | Disseny: <a href="https://sokvist.com" target="_blank">Sokvist</a>
                    </span>
                </div>
            </div>
            
        </div><!-- /wrapper-footer -->
        
        <a id="back-to-top" href="#"></a>
        
	</footer><!--  End Footer  -->
    

    <script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="assets/js/min/plugins.min.js"></script>
    <script src="assets/js/min/main.min.js"></script>

    <script>
        /*(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-47544981-1', 'sokvist.com');
        ga('send', 'pageview');*/
    </script>
    

</body>
</html>