<?php
$pageTitle = 'Experiències: Espais';
$bodyClass = 'espais';
include 'header.php';
?>

	
    <section class="billboard halfheight">
        <div class="noslider">
            <div class="overlay"></div>
            <div class="single-img bg-img-espais"></div>
        </div> <!-- /.noslider -->
    </section><!-- /.billboard  -->
    
    
    <main class="halfmargin">
        
        <section class="separator-header"></section>

        <section class="intro wrapper wrapper-margin">
            
            <h1>Espais</h1>
            
        </section><!--  End Features  -->


        <section class="page-wrapper">
            <div class="spotlight spotlight-full">
                <div class="container container-full">
                    <div class="content">
                        <h2>Sala gran</h2>
                        <h3><em>La teva inquietud, la fem realitat</em></h3>
                        <p>Sala funcional i polivalent on celebrar esdeveniments a la teva mida.</p>
                    </div>
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight">
                <div class="image">
                    <img src="assets/images/page-espais-sala_gran.jpg" alt="Edetària - Espais" width="900" height="540" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <dl>
                            <dt>Superfície:</dt>
                            <dd>280 m2 construïts (120 m2 útils)</dd>
                            
                            <dt>Dimensions:</dt>
                            <dd>16 m X 18 m contruïts (120 m2 útils)</dd>
                            
                            <dt>Preu:</dt>
                            <dd>A consultar</dd>
                        </dl>
                        <dl>
                            <dt><i>Banquet</i>:</dt>
                            <dd>100 - 120 pax.</dd>
                            
                            <dt><i>Conferència</i>:</dt>
                            <dd>120 pax.</dd>
                            
                            <dt><i>Teatre</i>:</dt>
                            <dd>150 pax.</dd>
                            
                            <dt><i>Còctel</i>:</dt>
                            <dd>200 pax.</dd>
                        </dl>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="separator-middle" id="d-on-ve-edetaria"></section>
        
        <section class="page-wrapper">
            <div class="spotlight spotlight-full">
                <div class="container container-full">
                    <div class="content">
                        <h2>Sala petita</h2>
                        <h3><em>Creativitat i funcionalitat</em></h3>
                        <p>Aquesta sala ofereix la possibilitat de combinar la teva reunió de treball amb un àpat, <i>coffee break</i>, o gaudir d’un <i>showcooking</i>.</p>
                    </div>
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight">
                <div class="image">
                    <img src="assets/images/page-espais-sala_petita.jpg" alt="Edetària - Espais" width="900" height="540" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <dl>
                            <dt>Superfície:</dt>
                            <dd>40 m2</dd>
                            
                            <dt>Dimensions:</dt>
                            <dd>5 m X 8 m</dd>
                            
                            <dt>Preu:</dt>
                            <dd>A consultar</dd>
                        </dl>
                        <dl>
                            <dt><i>Banquet</i>:</dt>
                            <dd>20 pax.</dd>
                            
                            <dt><i>Conferència</i>:</dt>
                            <dd>20 pax.</dd>
                            
                            <dt><i>Teatre</i>:</dt>
                            <dd>30 pax.</dd>
                        </dl>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="separator-middle" id="d-on-ve-edetaria"></section>
        
        <section class="page-wrapper">
            <div class="spotlight spotlight-full">
                <div class="container container-full">
                    <div class="content">
                        <h2>Terrassa a les vinyes</h2>
                        <h3><em>L’essència de la terra</em></h3>
                        <p>Espai per a reunions més personals o com a espai alternatiu al teu esdeveniment.</p>
                    </div>
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight">
                <div class="image">
                    <img src="assets/images/page-espais-terrassa.jpg" alt="Edetària - Espais" width="900" height="540" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <dl>
                            <dt>Superfície:</dt>
                            <dd>70 m2</dd>
                            
                            <dt>Dimensions:</dt>
                            <dd>7 m X 10 m</dd>
                            
                            <dt>Preu:</dt>
                            <dd>A consultar</dd>
                        </dl>
                        <dl>
                            <dt><i>Banquet</i>:</dt>
                            <dd>30 pax.</dd>
                            
                            <dt><i>Conferència</i>:</dt>
                            <dd>30 pax.</dd>
                            
                            <dt><i>Teatre</i>:</dt>
                            <dd>40 pax.</dd>
                        </dl>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="separator-middle" id="d-on-ve-edetaria"></section>
        
        <section class="page-wrapper">
            <div class="spotlight spotlight-full">
                <div class="container container-full">
                    <div class="content">
                        <h2>Oliveres</h2>
                        <h3><em>Identitat y Autenticitat</em></h3>
                        <p>Espai exterior per estar en contacte total amb la natura, envoltat de bosc i vinyes.</p>
                    </div>
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight">
                <div class="image">
                    <img src="assets/images/page-espais-oliveres.jpg" alt="Edetària - Espais" width="900" height="540" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <dl>
                            <dt>Superfície:</dt>
                            <dd> </dd>
                            
                            <dt>Dimensions:</dt>
                            <dd> </dd>
                            
                            <dt>Preu:</dt>
                            <dd>A consultar</dd>
                        </dl>
                        <dl>
                            <dt><i>Banquet</i>:</dt>
                            <dd>100 - 120 pax.</dd>
                            
                            <dt><i>Conferència</i>:</dt>
                            <dd>120 pax.</dd>
                            
                            <dt><i>Teatre</i>:</dt>
                            <dd>150 pax.</dd>
                            
                            <dt><i>Còctel</i>:</dt>
                            <dd>200 pax.</dd>
                        </dl>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="separator-middle" id="d-on-ve-edetaria"></section>
        
        <section class="page-wrapper">
            <div class="spotlight spotlight-full">
                <div class="container container-full">
                    <div class="content">
                        <h2>Mas</h2>
                        <h3><em>Maridatge d’oci i negoci</em></h3>
                        <p>Un espai singular per a celebrar la teva reunió de treball i gaudir d’un maridatge de productes autòctons amb els nostres vins.</p>
                    </div>
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight">
                <div class="image">
                    <img src="assets/images/page-espais-mas.jpg" alt="Edetària - Espais" width="900" height="520" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <dl>
                            <dt>Superfície:</dt>
                            <dd>32 m2</dd>
                            
                            <dt>Dimensions:</dt>
                            <dd>4 m X 8 m</dd>
                            
                            <dt>Preu:</dt>
                            <dd>A consultar</dd>
                        </dl>
                        <dl>
                            <dt><i>Còctel exterior</i>:</dt>
                            <dd>20 - 30 pax.</dd>
                            
                            <dt><i>Banquet interior</i>:</dt>
                            <dd>20 pax.</dd>
                        </dl>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="separator-middle" id="d-on-ve-edetaria"></section>
        
        <section class="page-wrapper">
            <img src="assets/images/page-espais-btm.jpg" alt="Edetària - Vinyes i terra" width="1900" height="521" />
        </section>
        
        <section class="page-wrapper separator"></section>
        
    </main>


<?php include("footer.php"); ?>