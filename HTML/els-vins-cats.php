<?php
$pageTitle = 'Els vins - Games';
$bodyClass = 'els-vins-cats';
include 'header.php';
?>

	
    <section class="billboard noheight">
        <div class="noslider">
            <div class="overlay"></div>
            <div class="single-img bg-img-el_celler"></div>
        </div> <!-- /.noslider -->
    </section><!-- /.billboard  -->
    
    
    <main class="nomargin">
        
        <section class="separator-header"></section>

        <section class="intro wrapper wrapper-margin">
            
            <h1>Els nostres vins: Via Terra</h1>
            <h2><em></em></h2>
            <p></p>
            
        </section><!--  End Features  -->


        <section class="page-wrapper">
            <div class="spotlight">
                <div class="image">
                    <div class="slider">
                        <div class="flexslider-page">
                            <ul class="slides">
                                <li><img src="assets/images/vins/via-terra-blanc.jpg" alt="Edetària - Via Terra Garnatxa Blanca" width="900" height="600" /></li>
                            </ul>
                        </div> <!-- /.flexslider-page -->
                    </div> <!-- /.slider -->
                </div>
                
                <div class="container">
                    <div class="content">
                        <h2 class="wine-title"><a href="els-vins-single.php">Via Terra <span>Blanc</span></a></h2>
                        <p>Elaborat amb Garnatxa blanca de les nostres vinyes situades a la Plana de Gandesa. Combinem dues veremes, una de primerenca que ens aportarà tota la frescor de la fruita i una dues setmanes més tard per obtenir la calidesa  de la garnatxa. Després de fermentar, s’ajuntaran ambdues i es treballen amb les seves  mares fines  per extreure tota la cremositat i delicadesa. El resultat és un vi original de Garnatxa blanca, fresc i gastronòmic.</p>
                        
                        <a href="els-vins-single.php" class="cta"><span>Comprar</span>
                            <svg width="10px" height="8px" viewBox="0 0 13 10">
                                <path d="M1,5 L11,5"></path>
                                <polyline points="8 1 12 5 8 9"></polyline>
                            </svg>
                        </a>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="separator-middle" id="un-estil-propi"></section>
        
        <section class="page-wrapper">
            <div class="spotlight reverse">
                <div class="image">
                    <div class="slider">
                        <div class="flexslider-page">
                            <ul class="slides">
                                <li><img src="assets/images/vins/via-terra-negre.jpg" alt="Edetària - Via Terra Garnatxa Blanca" width="900" height="600" /></li>
                            </ul>
                        </div> <!-- /.flexslider-page -->
                    </div> <!-- /.slider -->
                </div>
                
                <div class="container">
                    <div class="content">
                        <h2 class="wine-title"><a href="els-vins-single.php">Via Terra <span>Negre</span></a></h2>
                        <p>Elaborat amb Garnatxa negra de les nostres vinyes situades a la Plana de Gandesa, en terrasses elevades, sòls ben drenats i per tant donant baixes produccions.</p>
                        <p>En el celler realitzem dues vinificacions diferents, una amb maceració en fred per aportar tota l’autenticitat de la fruita de la Garnatxa negra; i una segona més tradicional amb raïms més madurs que ens donarà complexitat i estructura. Després el vi rep una criança de 6 mesos en bótes de 300 litres de roure francès.
                        </p>
                        
                        <a href="els-vins-single.php" class="cta"><span>Comprar</span>
                            <svg width="10px" height="8px" viewBox="0 0 13 10">
                                <path d="M1,5 L11,5"></path>
                                <polyline points="8 1 12 5 8 9"></polyline>
                            </svg>
                        </a>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="separator-middle" id="un-estil-propi"></section>
        
        <section class="page-wrapper">
            <div class="spotlight">
                <div class="image">
                    <div class="slider">
                        <div class="flexslider-page">
                            <ul class="slides">
                                <li><img src="assets/images/vins/via-terra-blanc.jpg" alt="Edetària - Via Terra Garnatxa Blanca" width="900" height="600" /></li>
                            </ul>
                        </div> <!-- /.flexslider-page -->
                    </div> <!-- /.slider -->
                </div>
                
                <div class="container">
                    <div class="content">
                        <h2 class="wine-title"><a href="els-vins-single.php">Via Terra <span>Rosat</span></a></h2>
                        <p>Elaborat amb la varietat autòctona Garnatxa peluda de les nostres vinyes més joves situades a la Plana de Gandesa.</p>
 
                        <p>Verema manual en caixes de 15 kg. Refredem el raïm durant 48 hores i després d’un desrapat suau, el macerem unes poques hores amb les pells a 5ºC, per obtenir el color tan característic a pell de ceba. Després de descubar fermentem a temperatura controlada de 16ºC. Acabada aquesta, realitzem un treball amb les lies fines per donar-li cremositat.</p>
                        
                        <a href="els-vins-single.php" class="cta"><span>Comprar</span>
                            <svg width="10px" height="8px" viewBox="0 0 13 10">
                                <path d="M1,5 L11,5"></path>
                                <polyline points="8 1 12 5 8 9"></polyline>
                            </svg>
                        </a>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        
        <section class="separator-middle" id="un-estil-propi"></section>
        
        <section class="wrapper wrapper-margin wrapper-aside">
            
            <h2 class="aside-title">Seleccioneu un vi</h2>
            
            <aside class="aside all-wines">
                <div class="widget">		
                    <h3 class="widget-title"><a href="els-vins-cats.php">Via <span>Terra</span></a></h3>		
                    <nav class="cat-nav">
                        <ul>
                            <li><a href="els-vins-single.php" class="cta"><span>Via Terra Blanc</span></a></li>
                            <li><a href="els-vins-single.php" class="cta"><span>Via Terra Negre</span></a></li>
                            <li><a href="els-vins-single.php" class="cta"><span>Via Terra Rosat</span></a></li>
                        </ul>
                    </nav>
                </div>
                <div class="widget">		
                    <h3 class="widget-title"><a href="els-vins-cats.php">Via <span>Edetana</span></a></h3>		
                    <nav class="cat-nav">
                        <ul>
                            <li><a href="els-vins-single.php" class="cta"><span>Via Edetana Blanc</span></a></li>
                            <li><a href="els-vins-single.php" class="cta"><span>Via Edetana Negre</span></a></li>
                        </ul>
                    </nav>
                </div>

                <div class="widget">
                    <h3 class="widget-title"><a href="els-vins-cats.php">Edetària <span>Selecció</span></a></h3>		
                    <nav class="cat-nav">
                        <ul>
                            <li><a href="els-vins-single.php" class="cta"><span>Edetària Selecció Blanc</span></a></li>
                            <li><a href="els-vins-single.php" class="cta"><span>Edetària Selecció Negre</span></a></li>
                            <li><a href="els-vins-single.php" class="cta"><span>Edetària Dolç</span></a></li>
                        </ul>
                    </nav>
                </div>
                
                <div class="widget">
                    <h3 class="widget-title"><a href="els-vins-cats.php">Les nostres <span>finques</span></a></h3>		
                    <nav class="cat-nav">
                        <ul>
                            <li><a href="els-vins-single.php" class="cta"><span>La Personal</span></a></li>
                            <li><a href="els-vins-single.php" class="cta"><span>La Pedrissa</span></a></li>
                            <li><a href="els-vins-single.php" class="cta"><span>La Genuïna</span></a></li>
                            <li><a href="els-vins-single.php" class="cta"><span>La Terrenal</span></a></li>
                        </ul>
                    </nav>
                </div>
            </aside>
        </section>
        
        
        <section class="page-wrapper separator"></section>
        
    </main>


<?php include("footer.php"); ?>