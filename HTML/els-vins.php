<?php
$pageTitle = 'Els vins';
$bodyClass = 'els-vins';
include 'header.php';
?>

	
    <section class="billboard noheight">
        <div class="noslider">
            <div class="overlay"></div>
            <div class="single-img bg-img-el_celler"></div>
        </div> <!-- /.noslider -->
    </section><!-- /.billboard  -->
    
    
    <main class="nomargin">
        
        <section class="separator-header"></section>

        <section class="intro wrapper wrapper-margin">
            
            <h1>Els vins</h1>
            <h2><em></em></h2>
            <p></p>
            
        </section><!--  End Features  -->


        <section class="page-wrapper">
            <div class="spotlight">
                <div class="image">
                    <div class="slider">
                        <div class="flexslider-page">
                            <ul class="slides">
                                <li><img src="assets/images/vins/via-terra-blanc.jpg" alt="Edetària - Via Terra Garnatxa Blanca" width="900" height="600" /></li>
                                <li><img src="assets/images/vins/via-terra-negre.jpg" alt="Edetària - Via Terra Garnatxa Negre" width="900" height="600" /></li>
                                <li><img src="assets/images/vins/via-terra-rosat.jpg" alt="Edetària - Via Terra Garnatxa Rosat" width="900" height="600" /></li>
                            </ul>
                        </div> <!-- /.flexslider-page -->
                    </div> <!-- /.slider -->
                </div>
                
                <div class="container">
                    <div class="content">
                        <h2 class="wine-title"><a href="els-vins-cats.php">Via <span>Terra</span></a></h2>
                        <p>Els Via Terra Selecció mostren el <em>caràcter de les nostres garnatxes</em> i reflecteixen la seva màxima fruitositat i puresa. Els colors de les garnatxes: la garnatxa blanca, la garnatxa negra i la garnatxa peluda, vins “<em>disfrutons</em>”, fàcils de beure i per passar-s’ho bé.</p>
                        <nav class="cat-nav">
                            <ul>
                                <li><a href="els-vins-single.php" class="cta"><span>Via Terra Blanc</span>
                                    <svg width="10px" height="8px" viewBox="0 0 13 10">
                                        <path d="M1,5 L11,5"></path>
                                        <polyline points="8 1 12 5 8 9"></polyline>
                                    </svg>
                                </a></li>
                                <li><a href="els-vins-single.php" class="cta"><span>Via Terra Negre</span>
                                    <svg width="10px" height="8px" viewBox="0 0 13 10">
                                        <path d="M1,5 L11,5"></path>
                                        <polyline points="8 1 12 5 8 9"></polyline>
                                    </svg>
                                </a></li>
                                <li><a href="els-vins-single.php" class="cta"><span>Via Terra Rosat</span>
                                    <svg width="10px" height="8px" viewBox="0 0 13 10">
                                        <path d="M1,5 L11,5"></path>
                                        <polyline points="8 1 12 5 8 9"></polyline>
                                    </svg>
                                </a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="separator-middle" id="un-estil-propi"></section>
        
        <section class="page-wrapper">
            <div class="spotlight reverse">
                <div class="image">
                    <div class="slider">
                        <div class="flexslider-page">
                            <ul class="slides">
                                <li><img src="assets/images/vins/via-edetana-blanc.jpg" alt="Edetària - Via Edetana Blanc" width="900" height="600" /></li>
                                <li><img src="assets/images/vins/via-edetana-negre.jpg" alt="Edetària - Via Edetana Negre" width="900" height="600" /></li>
                            </ul>
                        </div> <!-- /.flexslider-page -->
                    </div> <!-- /.slider -->
                </div>
                
                <div class="container">
                    <div class="content">
                        <h2 class="wine-title"><a href="els-vins-cats.php">Via <span>Edetana</span></a></h2>
                        <p>Els Via Edetana són l’òptima combinació de fruita garnatxera i el camí per mostrar la mineralitat de les nostres vinyes velles i els terrers d’on procedeixen. En resulten els vins més <em>internacionals</em>, amb la incorporació de Viognier i Syrah i els més gastronòmics, ideals per prendre durant els àpats.</p>
                        <nav class="cat-nav">
                            <ul>
                                <li><a href="els-vins-single.php" class="cta"><span>Via Edetana Blanc</span>
                                    <svg width="10px" height="8px" viewBox="0 0 13 10">
                                        <path d="M1,5 L11,5"></path>
                                        <polyline points="8 1 12 5 8 9"></polyline>
                                    </svg>
                                </a></li>
                                <li><a href="els-vins-single.php" class="cta"><span>Via Edetana Negre</span>
                                    <svg width="10px" height="8px" viewBox="0 0 13 10">
                                        <path d="M1,5 L11,5"></path>
                                        <polyline points="8 1 12 5 8 9"></polyline>
                                    </svg>
                                </a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="separator-middle" id="un-estil-propi"></section>
        
        <section class="page-wrapper">
            <div class="spotlight">
                <div class="image">
                    <div class="slider">
                        <div class="flexslider-page">
                            <ul class="slides">
                                <li><img src="assets/images/vins/edetaria-seleccio-blanc.jpg" alt="Edetària - Edetària Selecció Blanc" width="900" height="600" /></li>
                                <li><img src="assets/images/vins/edetaria-seleccio-negre.jpg" alt="Edetària - Edetària Selecció Negre" width="900" height="600" /></li>
                                <li><img src="assets/images/vins/edetaria-dols.jpg" alt="Edetària - Edetària Dolç" width="900" height="600" /></li>
                            </ul>
                        </div> <!-- /.flexslider-page -->
                    </div> <!-- /.slider -->
                </div>
                
                <div class="container">
                    <div class="content">
                        <h2 class="wine-title"><a href="els-vins-cats.php">Edetària <span>Selecció</span></a></h2>
                        <p>Els Edetària Seleció són l’expressió més autèntica i elegant de les nostres garnatxes. Màxima expressió dels nostres sòls, varietats i Mediterraneïtat: el nostre estàndard.</p>
                        <nav class="cat-nav">
                            <ul>
                                <li><a href="els-vins-single.php" class="cta"><span>Edetària Selecció Blanc</span>
                                    <svg width="10px" height="8px" viewBox="0 0 13 10">
                                        <path d="M1,5 L11,5"></path>
                                        <polyline points="8 1 12 5 8 9"></polyline>
                                    </svg>
                                </a></li>
                                <li><a href="els-vins-single.php" class="cta"><span>Edetària Selecció Negre</span>
                                    <svg width="10px" height="8px" viewBox="0 0 13 10">
                                        <path d="M1,5 L11,5"></path>
                                        <polyline points="8 1 12 5 8 9"></polyline>
                                    </svg>
                                </a></li>
                                <li><a href="els-vins-single.php" class="cta"><span>Edetària Dolç</span>
                                    <svg width="10px" height="8px" viewBox="0 0 13 10">
                                        <path d="M1,5 L11,5"></path>
                                        <polyline points="8 1 12 5 8 9"></polyline>
                                    </svg>
                                </a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="separator-middle" id="un-estil-propi"></section>
        
        <section class="page-wrapper">
            <div class="spotlight reverse">
                <div class="image">
                    <div class="slider">
                        <div class="flexslider-page">
                            <ul class="slides">
                                <li><img src="assets/images/vins/finca-la-personal.jpg" alt="Edetària - Finca La Personal" width="900" height="600" /></li>
                                <li><img src="assets/images/vins/finca-la-pedrissa.jpg" alt="Edetària - Finca La Pedrissa" width="900" height="600" /></li>
                                <li><img src="assets/images/vins/finca-la-genuina.jpg" alt="Edetària - Finca La Genuïna" width="900" height="600" /></li>
                                <li><img src="assets/images/vins/finca-la-terrenal.jpg" alt="Edetària - Finca La Terranal" width="900" height="600" /></li>
                            </ul>
                        </div> <!-- /.flexslider-page -->
                    </div> <!-- /.slider -->
                </div>
                
                <div class="container">
                    <div class="content">
                        <h2 class="wine-title"><a href="els-vins-cats.php">Les nostres <span>finques</span></a></h2>
                        <p>Vins elaborats a partir de parcel•les úniques, cadascuna amb la seva varietat autòctona, el seu terrer específic i de vinyes molt velles, per obtenir la màxima singularitat i finesa de la Terra Alta.</p>
                        <nav class="cat-nav">
                            <ul>
                                <li><a href="els-vins-single.php" class="cta"><span>La Personal</span>
                                    <svg width="10px" height="8px" viewBox="0 0 13 10">
                                        <path d="M1,5 L11,5"></path>
                                        <polyline points="8 1 12 5 8 9"></polyline>
                                    </svg>
                                </a></li>
                                <li><a href="els-vins-single.php" class="cta"><span>La Pedrissa</span>
                                    <svg width="10px" height="8px" viewBox="0 0 13 10">
                                        <path d="M1,5 L11,5"></path>
                                        <polyline points="8 1 12 5 8 9"></polyline>
                                    </svg>
                                </a></li>
                                <li><a href="els-vins-single.php" class="cta"><span>La Genuïna</span>
                                    <svg width="10px" height="8px" viewBox="0 0 13 10">
                                        <path d="M1,5 L11,5"></path>
                                        <polyline points="8 1 12 5 8 9"></polyline>
                                    </svg>
                                </a></li>
                                <li><a href="els-vins-single.php" class="cta"><span>La Terrenal</span>
                                    <svg width="10px" height="8px" viewBox="0 0 13 10">
                                        <path d="M1,5 L11,5"></path>
                                        <polyline points="8 1 12 5 8 9"></polyline>
                                    </svg>
                                </a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="page-wrapper separator"></section>
        
    </main>


<?php include("footer.php"); ?>