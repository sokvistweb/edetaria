<?php
$pageTitle = 'Un estil propi';
$bodyClass = 'edetaria';
include 'header.php';
?>

	
    <section class="billboard halfheight">
        <div class="noslider">
            <div class="overlay"></div>
            <div class="single-img bg-img-edetaria"></div>
        </div> <!-- /.noslider -->
    </section><!-- /.billboard  -->
    
    
    <main class="halfmargin">
        
        <section class="separator-header"></section>
        
        <section class="intro wrapper wrapper-margin" id="qui-som">
            
            <h1>Edetària</h1>
            <h2><em>Un estil propi</em></h2>
            
        </section><!--  End Features  -->

        <section class="page-wrapper">
            <div class="spotlight">
                <div class="image">
                    <img src="assets/images/page-edetaria-qui_som-1.jpg" alt="Edetària - Qui som" width="900" height="520" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <h2>Qui som</h2>

                        <p>Joan Àngel Lliberia, ànima d’Edetària emprengué aquesta aventura després d’estudiar Enginyer Agrònom i de cursar el màster en Direcció d’Empreses del Sector del Vi a l’OIV (Office International de la Vigne et du vin); però primer treballà com a directiu en vàries empreses del sector a França i després a Catalunya, i en multinacionals de diferents sectors.</p>
                        <div class="separator-hover1"></div>
                    </div>
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight reverse">
                <div class="container">
                    <div class="content">
                        <p>Segurament per la infantesa viscuda al voltant d’aquest apassionant món, tornà als seus orígens per a engegar Edetària, un somni ara fet realitat, com homenatge al seu avi Llorens -enòleg- i el seus pares Pepita i Àngel -viticultors-, elaborant vins autèntics a la seva terra, amb les vinyes de casa i amb un estil propi.</p>
                        <div class="separator-hover1"></div>
                    </div>
                </div>
                
                <div class="image">
                    <img src="assets/images/page-edetaria-qui_som-2.jpg" alt="Edetària - Qui som" width="900" height="520" />
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="separator-middle" id="un-estil-propi"></section>
        
        <section class="page-wrapper">
            <div class="spotlight">
                <div class="image">
                    <img src="assets/images/page-edetaria-estil_propi-1.jpg" alt="Edetària - Un estil propi" width="900" height="520" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <h2>Un estil propi</h2>

                        <p>A Edetària elaborem vins autèntics amb raïms de varietats autòctones, cercant la màxima expressió dels nostres terrers, recuperant el valor de les vinyes velles i treballant de manera ecològica.</p>
                        <div class="separator-hover1"></div>
                    </div>
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight reverse">
                <div class="container">
                    <div class="content">
                        <h3>L’expressió del terrer</h3>
                        <p>El vi està lligat a una terra i ha d’ésser mostra de la seva expressió en un sentit ampli: sòl, clima i manera de fer. Per tant som absolutament respectuosos amb la natura, treballem de manera sostenible i cerquem la màxima expressió i autenticitat de la nostra terra.</p>
                        <div class="separator-hover1"></div>
                    </div>
                </div>
                
                <div class="image">
                    <img src="assets/images/page-edetaria-estil_propi-2.jpg" alt="Edetària - Un estil propi" width="900" height="520" />
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight">
                <div class="image">
                    <img src="assets/images/page-edetaria-estil_propi-3.jpg" alt="Edetària - Un estil propi" width="900" height="520" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <h3>La creativitat</h3>

                        <p>El vi alhora és reflex de qui el pensa, el dissenya i l’elabora. Elaborem vins amb caràcter propi, coneixem bé les condicions, apliquem el procés més rigorós, però alhora ens agrada deixar un lloc per a la “improvisació”, pel neguit de crear, d’experimentar sense cap altre condicionant.</p>
                        <div class="separator-hover1"></div>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="separator-middle" id="territori"></section>
        
        <section class="page-wrapper">
            <div class="spotlight reverse">
                <div class="image">
                    <img src="assets/images/page-edetaria-territori-1.jpg" alt="Edetària - Territori" width="900" height="520" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <h2>Territori</h2>

                        <p>Ens trobareu a la zona central de la Terra Alta, a Gandesa, un dels dotze municipis que conformen aquest territori de gran varietat paisatgística.</p>
                        
                        <div class="separator-hover1"></div>
                    </div>
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight">
                <div class="image">
                    <img src="assets/images/page-edetaria-territori-2.jpg" alt="Edetària - Territori" width="900" height="520" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <p>L’economia d’aquesta comarca està basada en l’agricultura, fonamentalment cultius de fruits secs, oliveres, i molt especialment en el conreu de la vinya i la indústria vitivinícola.</p>
                        
                        <p>En l’actualitat la Terra Alta és la segona comarca vitícola de Catalunya en superfície de vinya, a més a més 1/3 de la Garnatxa blanca mundial està plantada aquí, tota una declaració d’intensions.</p>
                        <div class="separator-hover1"></div>
                    </div>
                </div>
            </div><!-- /.spotlight -->
            
            <img src="assets/images/page-edetaria-territori-3-large.jpg" alt="Edetària - Territori" width="1900" height="600" />
        </section>
        
        <section class="separator-middle" id="d-on-ve-edetaria"></section>
        
        <section class="page-wrapper">
            <div class="spotlight reverse">
                <div class="image">
                    <img src="assets/images/page-edetaria-via_edetaria-1.jpg" alt="Edetària - D'on ve Via Edetària" width="900" height="580" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <h2>D’on ve Edetària</h2>
                        <p>El nostre nom es fa ressò de la "Via Edetana", antiga via romana que unia l'actual Tortosa amb Saragossa. Amb això volem retre homenatge a la cultura mediterrània de la qual formem part, i on el vi ha estat i és un element central.</p>
                        
                        <p>Ibers laietans, cossetans, ilergetes, sedetans, ilercavons i edetans ja poblaven la Terra Alta abans de la romanització, i deixaren vestigis com l’emplaçament de producció vinícola més antic de Catalunya, en el trull del jaciment del Coll del Moro de Gandesa.</p>
                        <div class="separator-hover1"></div>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="separator-middle" id="equip"></section>
        
        <section class="page-wrapper">
            <div class="spotlight">
                <div class="image">
                    <img src="assets/images/page-edetaria-equip-1.jpg" alt="Edetària - L'equip" width="900" height="520" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <h2>L’equip</h2>
                        <p>L’altre actiu clau d’Edetària és l’equip. Tots gaudim del treball del dia a dia, apassionats per oferir el millor de nosaltres i de la nostra terra, sabedors que d’una manera màgica quan algú obre una de les nostres ampolles ho percebrà i en gaudirà.</p>
                        <div class="separator-hover1"></div>
                    </div>
                </div>
            </div><!-- /.spotlight -->

            <img src="assets/images/page-edetaria-equip-2-large.jpg" alt="Edetària - L'equip" width="1900" height="600" />
        </section>
        
        <section class="page-wrapper separator"></section>
        
    </main>


<?php include("footer.php"); ?>