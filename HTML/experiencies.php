<?php
$pageTitle = 'Experiències';
$bodyClass = 'experiencies';
include 'header.php';
?>

	
    <section class="billboard halfheight">
        <div class="noslider">
            <div class="overlay"></div>
            <div class="single-img bg-img-experiencies"></div>
        </div> <!-- /.noslider -->
    </section><!-- /.billboard  -->
    
    
    <main class="halfmargin">
        
        <section class="separator-header"></section>

        <section class="intro wrapper wrapper-margin">
            
            <h1>Experiències</h1>
            <h2><em>L’expressió d’un territori, l’esperit d’una terra.</em></h2>
            
        </section><!--  End Features  -->


        <section class="page-wrapper">
            <div class="spotlight grid">
                <div class="container33">
                    <figure class="effect-skv3 image">
                        <img class="large-img" src="assets/images/experiencies-enoturisme.jpg" alt="Edetària" width="900" height="460" />
                        <img class="small-img" src="assets/images/experiencies-enoturisme-2.jpg" alt="Edetària" width="900" height="520" />
                        <figcaption>
                            <div class="fig-content">
                                <h2>Enoturisme</h2>
                                <ul>
                                    <li><a href="enoturisme.php#passeig-entre-vinyes">Passeig entre vinyes</a></li>
                                    <li><a href="enoturisme.php#experiencia-gourmet">Experiència Gourmet</a></li>
                                    <li><a href="enoturisme.php#els-top">Els nostres TOP's</a></li>
                                    <li><a href="enoturisme.php#creativa">Creativa</a></li>
                                    <li><a href="enoturisme.php#nens">Nens</a></li>
                                    <li><a href="enoturisme.php#escoltant-les-vinyes">Escoltant les vinyes</a></li>
                                    <li><a href="enoturisme.php#crea-la-teva-experiencia">Crea la teva experiència</a></li>
                                </ul>
                            </div>
                            <a href="enoturisme.php">Veure més</a>
                        </figcaption>
                    </figure>
                </div>
                
                <div class="container33">
                    <figure class="effect-skv3 image">
                        <img class="large-img" src="assets/images/experiencies-empreses.jpg" alt="Edetària" width="900" height="460" />
                        <img class="small-img" src="assets/images/experiencies-empreses-2.jpg" alt="Edetària" width="900" height="520" />
                        <figcaption>
                            <div class="fig-content">
                                <h2>Empreses</h2>
                                <p><em>Deixa de planificar reunions, comença a dissenyar experiències</em></p>
                            </div>
                            <a href="empreses.php">Veure més</a>
                        </figcaption>
                    </figure>
                </div>
                
                <div class="container33">
                    <figure class="effect-skv3 image">
                        <img class="large-img" src="assets/images/experiencies-espais.jpg" alt="Edetària" width="900" height="460" />
                        <img class="small-img" src="assets/images/experiencies-espais-2.jpg" alt="Edetària" width="900" height="520" />
                        <figcaption>
                            <div class="fig-content">
                                <h2>Espais</h2>
                                <p><em>La teva inquietud, la fem realitat</em></p>
                            </div>
                            <a href="espais.php">Veure més</a>
                        </figcaption>
                    </figure>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="page-wrapper separator"></section>
        
    </main>


<?php include("footer.php"); ?>