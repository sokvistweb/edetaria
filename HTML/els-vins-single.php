<?php
$pageTitle = 'Els vins - Single';
$bodyClass = 'els-vins-single';
include 'header.php';
?>

	
    <section class="billboard noheight">
        <div class="noslider">
            <div class="overlay"></div>
            <div class="single-img bg-img-el_celler"></div>
        </div> <!-- /.noslider -->
    </section><!-- /.billboard  -->
    
    
    <main class="nomargin">
        
        <section class="separator-header"></section>

        <section class="page-wrapper">
            <div class="spotlight">
                <div class="image">
                    <div class="slider">
                        <div class="flexslider-page">
                            <ul class="slides">
                                <li><img src="assets/images/vins/via-terra-blanc.jpg" alt="Edetària - Via Terra Garnatxa Blanca" width="900" height="600" /></li>
                            </ul>
                        </div> <!-- /.flexslider-page -->
                    </div> <!-- /.slider -->
                </div>
                
                <div class="container">
                    <div class="content">
                        <h1 class="product_title entry-title">Via Terra <span>Blanc</span></h1>
                        
                        <p class="price"><span class="woocommerce-Price-amount amount">40,00<span class="woocommerce-Price-currencySymbol">€</span></span></p>
                        
                        <form class="cart" method="post" enctype="multipart/form-data">
                            
                               <div class="quantity">
                                <label class="screen-reader-text" for="quantity_5c4b31e254a4e">Cantidad</label>
                                <input type="number" id="quantity_5c4b31e254a4e" class="input-text qty text" step="1" min="1" max="" name="quantity" value="1" title="Cantidad" size="4" pattern="[0-9]*" inputmode="numeric" aria-labelledby="">
                            </div>
	
                            <button type="submit" name="add-to-cart" value="41" class="add-to-cart single_add_to_cart_button button cta"><span>Afegir al carret</span></button>

                        </form>
                        
                        <div class="share-icons">
                            <p>Comparteix</p>
                            <ul class="icons">
                                <li>
                                    <a href="#" title="Comparte en Twitter" class="twitter" target="_blank">
                                        <svg class="icon"><use xlink:href="assets/images/symbol-defs.svg#icon-social-twitter"></use></svg>
                                        <span class="label">Twitter</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="facebook" target="_blank">
                                        <svg class="icon"><use xlink:href="assets/images/symbol-defs.svg#icon-social-facebook"></use></svg>
                                        <span class="label">Facebook</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="Comparte en Google+" class="googleplus" target="_blank">
                                        <svg class="icon"><use xlink:href="assets/images/symbol-defs.svg#icon-social-instagram"></use></svg>
                                        <span class="label">Google+</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" data-action="share/whatsapp/share" title="Comparte en WhatsApp" class="whatsapp" target="_blank">
                                        <svg class="icon"><use xlink:href="assets/images/symbol-defs.svg#icon-social-whatsapp"></use></svg>
                                        <span class="label">WhatsApp</span>
                                    </a>
                                </li>
                            </ul>
                        </div><!-- /.share-icons -->
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="wrapper wrapper-margin">
            <article class="entry-content">
                <p>Elaborat amb Garnatxa blanca de les nostres vinyes situades a la Plana de Gandesa. Combinem dues veremes, una de primerenca que ens aportarà tota la frescor de la fruita i una dues setmanes més tard per obtenir la calidesa  de la garnatxa. Després de fermentar, s’ajuntaran ambdues i es treballen amb les seves  mares fines  per extreure tota la cremositat i delicadesa. El resultat és un vi original de Garnatxa blanca, fresc i gastronòmic.</p>
            </article>
            
            <article class="entry-content">
                <h2>Tast</h2>
                <p>Aromes a fruita fresca: pera i meló. L’entrada en boca és potent, amb explosió de fruites, golós i final cítric que el fa persistent.</p>
            </article>
        </section>
        
        <section class="separator-header"></section>
        
        <section class="wrapper wrapper-margin wrapper-aside">
            
            <h2 class="aside-title">Seleccioneu un vi</h2>
            
            <aside class="aside all-wines">
                <div class="widget">		
                    <h3 class="widget-title"><a href="els-vins-cats.php">Via <span>Terra</span></a></h3>		
                    <nav class="cat-nav">
                        <ul>
                            <li><a href="els-vins-single.php" class="cta"><span>Via Terra Blanc</span></a></li>
                            <li><a href="els-vins-single.php" class="cta"><span>Via Terra Negre</span></a></li>
                            <li><a href="els-vins-single.php" class="cta"><span>Via Terra Rosat</span></a></li>
                        </ul>
                    </nav>
                </div>
                <div class="widget">		
                    <h3 class="widget-title"><a href="els-vins-cats.php">Via <span>Edetana</span></a></h3>		
                    <nav class="cat-nav">
                        <ul>
                            <li><a href="els-vins-single.php" class="cta"><span>Via Edetana Blanc</span></a></li>
                            <li><a href="els-vins-single.php" class="cta"><span>Via Edetana Negre</span></a></li>
                        </ul>
                    </nav>
                </div>

                <div class="widget">
                    <h3 class="widget-title"><a href="els-vins-cats.php">Edetària <span>Selecció</span></a></h3>		
                    <nav class="cat-nav">
                        <ul>
                            <li><a href="els-vins-single.php" class="cta"><span>Edetària Selecció Blanc</span></a></li>
                            <li><a href="els-vins-single.php" class="cta"><span>Edetària Selecció Negre</span></a></li>
                            <li><a href="els-vins-single.php" class="cta"><span>Edetària Dolç</span></a></li>
                        </ul>
                    </nav>
                </div>
                
                <div class="widget">
                    <h3 class="widget-title"><a href="els-vins-cats.php">Les nostres <span>finques</span></a></h3>		
                    <nav class="cat-nav">
                        <ul>
                            <li><a href="els-vins-single.php" class="cta"><span>La Personal</span></a></li>
                            <li><a href="els-vins-single.php" class="cta"><span>La Pedrissa</span></a></li>
                            <li><a href="els-vins-single.php" class="cta"><span>La Genuïna</span></a></li>
                            <li><a href="els-vins-single.php" class="cta"><span>La Terrenal</span></a></li>
                        </ul>
                    </nav>
                </div>
            </aside>
        </section>
        
        <section class="page-wrapper separator"></section>
        
    </main>


<?php include("footer.php"); ?>