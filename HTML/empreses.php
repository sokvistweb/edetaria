<?php
$pageTitle = 'Experiències: Empreses';
$bodyClass = 'empreses';
include 'header.php';
?>

	
    <section class="billboard halfheight">
        <div class="noslider">
            <div class="overlay"></div>
            <div class="single-img bg-img-empreses"></div>
        </div> <!-- /.noslider -->
    </section><!-- /.billboard  -->
    
    
    <main class="halfmargin">
        
        <section class="separator-header"></section>

        <section class="intro wrapper wrapper-margin">
            
            <h1>Empreses</h1>
            <h2><em>Deixa de planificar reunions, comença a dissenyar experiències</em></h2>
            <p>Converteix-te en líder mitjançant el teu esdeveniment. Tens com objectiu fidelitzar, motivar, conèixer i relacionar-te amb el teu públic?</p>
            <p>A Edetària tens l’escenari perfecte per a mostrar la teva estratègia de comunicació.</p>
            
        </section><!--  End Features  -->


        <section class="page-wrapper">
            <div class="spotlight">
                <div class="image">
                    <img src="assets/images/page-empreses-1.jpg" alt="Edetària - Empreses" width="900" height="540" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <p>Beneficis:</p>
                        <ol>
                            <li>Augmenta la notorietat de la marca.</li>
                            <li>Ajuda a donar visibilitat i millorar la reputació de la marca.</li>
                            <li>Comunicació molt directa.</li>
                            <li>Crea impacte.</li>
                            <li>Fidelitza.</li>
                        </ol>
                        <div class="separator-hover1"></div>
                    </div>
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight reverse">
                <div class="image">
                    <img src="assets/images/page-empreses-2.jpg" alt="Edetària - Empreses" width="900" height="540" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <p><strong>Congressos, Conferències, Reunions de treball</strong></p>
                        <p>Un espai singular per a connectar amb la natura i crear sinergies perquè <em>“el tot és més gran que la suma de les parts”</em>.</p>
                        <p>Descansar la ment és necessari per a crear i innovar. Per això et proposem de combinar la teva reunió amb una activitat d’enoturisme a la teva mida.</p>
                        <div class="separator-hover1"></div>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="page-wrapper separator"></section>
        
    </main>


<?php include("footer.php"); ?>