<?php
$pageTitle = 'Vinyes i terra';
$bodyClass = 'vinyes-i-terra';
include 'header.php';
?>

	
    <section class="billboard halfheight">
        <div class="noslider">
            <div class="overlay"></div>
            <div class="single-img bg-img-vinyes_i_terra"></div>
        </div> <!-- /.noslider -->
    </section><!-- /.billboard  -->
    
    
    <main class="halfmargin">
        
        <section class="separator-header"></section>

        <section class="intro wrapper wrapper-margin">
            
            <h1>Vinyes i terra</h1>
            <h2>La vinya d'Edetària</h2>
            <h2><em>La vinya: el nostre valor més important</em></h2>
            <p>Edetària elabora vi només dels ceps i les vinyes de la seva propietat. L’objectiu és controlar tot el procés des del principi, per això, Edetària disposa actualment de 60 ha de vinya.  Cada vinya aporta el seu caràcter propi, que depèn de les característiques del sòl, de la situació, de l’orientació i de l’edat de la vinya. Una bona part de vinya és vella, amb més de 60 anys i la resta d’edat intermèdia entre 25 i 40 anys. Amb una de les vinyes més ben conreades de la contrada, cerquem produccions baixes.</p>
            
        </section><!--  End Features  -->


        <section class="page-wrapper">
            <div class="spotlight">
                <div class="image">
                    <div class="slider">
                        <div class="flexslider-page">
                            <ul class="slides">
                                <li><img src="assets/images/page-vinyes-els-sols-1.jpg" alt="Edetària - Vinyes i terra" width="900" height="540" /></li>
                                <li><img src="assets/images/page-vinyes-els-sols-2.jpg" alt="Edetària - El Celler" width="900" height="540" /></li>
                                <li><img src="assets/images/page-vinyes-els-sols-3.jpg" alt="Edetària - El Celler" width="900" height="540" /></li>
                            </ul>
                        </div> <!-- /.flexslider-page -->
                    </div> <!-- /.slider -->
                </div>
                
                <div class="container">
                    <div class="content">
                        <h2>Els sòls</h2>
                        <h3><em>La nostra personalitat, allò que ens diferencia</em></h3>
                        <p>A Edetària tenim plantades les nostres vinyes en cinc terrers diferents.</p>
                        <ul>
                        <li><b>“Panal”</b> duna fòssil del quaternari.</li>
                        <li><b>“Tapàs”</b> sòls de textura franca amb material subjacent argilós.</li>
                        <li><b>“Tapàs blanc”</b> sòls poc profunds amb fragments carbonats.</li>
                        <li><b>“Còdols”</b> antiga llera d’un riu, amb pedregositat superficial.</li>
                        <li><b>“Vall”</b> sòls llimosos, fèrtils i profunds.</li>
                        </ul>
                        <div class="separator-hover1"></div>
                    </div>
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight reverse">
                <div class="container">
                    <div class="content">
                        <p>La interacció de les diferents viníferes amb cada terrer dona com a resultat uns raïms diferents, cadascun amb la seva pròpia identitat. Això és el que ens diferencia; aquesta és la nostra personalitat.
                        </p>
                        <div class="separator-hover1"></div>
                    </div>
                </div>
                
                <div class="image">
                    <img src="assets/images/page-vinyes-els-raims-1.jpg" alt="Edetària - Vinyes i terra" width="900" height="540" />
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="separator-middle" id="un-estil-propi"></section>
        
        <section class="page-wrapper">
            <div class="spotlight">
                <div class="image">
                    <img src="assets/images/page-vinyes-els-raims-2.jpg" alt="Edetària - Vinyes i terra" width="900" height="540" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <h2>Les varietats de raïm</h2>
                        <h3><em>Som autòctons</em></h3>
                        <p>Treballem majoritàriament amb varietats autòctones, a més a més, gràcies a la nostra experiència, coneixem les condicions que cadascuna necessita. Cada vinya té la seva pròpia personalitat, que depèn principalment de les característiques del sòl, de la seva situació i orientació, i per tant parlem dels diferents raïms sempre  en relació a la vinya on creixen.</p>
                        <div class="separator-hover1"></div>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="separator-middle" id="un-estil-propi"></section>
        
        <section class="page-wrapper">
            <div class="spotlight reverse">
                <div class="image">
                    <img src="assets/images/page-vinyes-ecologics-1.jpg" alt="Edetària - Vinyes i terra" width="900" height="540" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <h2>Conreu ecològic</h2>
                        <h3><em>Som verds...</em></h3>
                        <p>Des de sempre hem treballat de manera sostenible per obtenir la màxima expressió i autenticitat de la nostra terra:</p>
                        <ul>
                        <li>Per mantenir l’estructura del sòl i oxigenar la terra, alternem el llaurar la vinya amb l’ús de cobertes vegetals.</li>
                        <li>Utilitzem adobs verds (restes de poda de la vinya i sega de cobertes vegetals) i adobs orgànics de diversos animals.</li>
                        <li>Evitem totalment l’ús de fungicides químics i d’insecticides.</li>
                        </ul>
                        <div class="separator-hover1"></div>
                    </div>
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight">
               <div class="image">
                    <img src="assets/images/page-vinyes-ecologics-2.jpg" alt="Edetària - Vinyes i terra" width="900" height="540" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <p>Ara a més a més ho hem certificat pel CCPAE.</p>
                        <p>Al celler també treballem de manera sostenible i ecològica, a tall d’exemple dir que el celler s’alimenta exclusivament de la seva planta d’energia solar.</p>
                        <div class="separator-hover1"></div>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="separator-middle" id="un-estil-propi"></section>
        
        <section class="page-wrapper">
            <div class="spotlight reverse">
                <div class="image">
                    <img src="assets/images/page-vinyes-experimentals-1.jpg" alt="Edetària - Vinyes i terra" width="900" height="540" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <h2>Projectes experimentals</h2>
                        <h3><em>Som tafaners...</em></h3>
                        <h4>Reserva de la Biosfera i Sostenibilitat</h4>
                        <p>Com a membres actius de Terres de l’Ebre Reserva de la Biosfera, volem posar en valor els recursos agraris, ambientals, paisatgístics i culturals, tot desenvolupant les activitats agràries i turístiques al territori.</p>
                        <div class="separator-hover1"></div>
                    </div>
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight">
               <div class="image">
                    <img src="assets/images/page-vinyes-experimentals-2.jpg" alt="Edetària - Vinyes i terra" width="900" height="540" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <h4>Plantació experimental de reg a la vinya  a la Finca la Serra d’Edetària amb l’Incavi</h4>
                        <p>Estudi de l’ús eficient de l’aigua en el reg a la vinya per a la producció de raïm de qualitat.</p>
                        
                        <h4>Salvaguarda Fenotips de varietats autòctones: Vitis Navarra.</h4>
                        <p>Selecció massal i multiplicació dels fenotips de Garnatxa blanca, Garnatxa fina, Garnatxa peluda i Carinyena de la Terra Alta.</p>
                        <div class="separator-hover1"></div>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="separator-middle" id="d-on-ve-edetaria"></section>
        
        <section class="page-wrapper">
            <img src="assets/images/page-vinyes_i_terra-btm.jpg" alt="Edetària - Vinyes i terra" width="1900" height="600" />
        </section>
        
        <section class="page-wrapper separator"></section>
        
    </main>


<?php include("footer.php"); ?>