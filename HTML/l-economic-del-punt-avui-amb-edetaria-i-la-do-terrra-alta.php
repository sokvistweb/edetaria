<?php
$pageTitle = 'L’Econòmic del Punt Avui amb Edetària';
$bodyClass = 'l-economic-del-punt-avui-amb-edetaria-i-la-do-terrra-alta';
include 'header.php';
?>

	
    <section class="billboard noheight">
        <div class="noslider">
            <div class="overlay"></div>
            <div class="single-img bg-img-noticies"></div>
        </div> <!-- /.noslider -->
    </section><!-- /.billboard  -->
    
    
    <main class="nomargin">
        
        <section class="separator-header"></section>

        <section class="intro wrapper wrapper-margin">
            
            <h1>Notícies</h1>
            
        </section><!--  End Features  -->
        
        
        <section class="wrapper wrapper-margin">
            <div class="spotlight">
                <div class="image">
                    <img src="assets/images/blog/img-05.jpg" alt="Edetària - Enoturisme" width="800" height="530" />
                </div>
                
                <div class="container">
                    <article class="content post">
                        <div class="entry-header">
                            <h1 class="entry-title">L'Econòmic del Punt Avui amb Edetària i la DO Terrra Alta
                            </h1>
                            <div class="entry-meta single-post-meta">
                                <span class="post-category"><a href="#">Premsa, Premis</a></span>

                                <span class="post-date"><a href="#"><time class="entry-date" datetime="2012-11-09T23:15:57+00:00">2 de setembre de 2018</time></a></span>

                                <span class="post-author"><a href="#">Laura Pinto</a></span>
                            </div>
                        </div>
                    </article>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        
        <section class="wrapper wrapper-margin20">
            <article class="entry-content">
               
                <p>Entrevista a Joan Àngel Lliberia al semanari <a href="http://www.leconomic.cat/neco/article/4-economia/18-economia/920480-.html">L'Econòmic del Punta Avui</a>. Llegeix tota la informació.</p>
                <p>Premi pel cep més personal d'Edetària</p>
                <p><strong>El celler de la Terra Alta és reconegut amb el guardó de millor vi de Catalunya</strong></p>
                <p>06/12/15 02:00 - TARRAGONA - <a href="mailto:mrovira@leconomic.cat">MARC ROVIRA</a></p>
                <p>Joan Àngel Lliberia reivindica: “A la Terra Alta ja se sap que es fan vins blancs molt interessants però també ens hem de creure que sabem fer vins negres. I no cal usar cabernets ni sirà per fer-los.” Ho diu des del reconeixement que li reporta haver estat escollit com l'elaborador del millor vi negre de l'any a Catalunya. La Personal del celler Edetària ha estat, segons la Guia de Vins de Catalunya, el millor d'entre més de 1.300 vins portats a concurs. Només el Finca Mas la Rosa de Vall Llach ha obtingut una puntuació similar.</p>
                <p>Amb una trajectòria de només 10 anys i apostant obertament per la vinya vella i les garnatxes, una varietat de raïm autòctona, Edetària s'ha situat com un dels cellers amb més crèdit del país. Al seu currículum, hi consta també un premi Peñín. “Els reconeixements sempre són benvinguts i representen una pujada de moral per a tota la gent que treballa a la vinya i al celler”, explica l'ideòleg d'Edetària.</p>
                <p>El vi La Personal arrenca com un projecte agosarat per buscar “un pas més enllà de l'Edetària Selecció”. L'origen de La Personal és en els poc més d'un miler de ceps de garnatxa peluda que la família Lliberia va plantar fa més de cinquanta anys. “Si jo planto vinya ara no faré un bon vi fins d'aquí a 25 anys”, sentencia el premiat cellerer. En aquest sentit, Edetària acaba de lligar l'arrendament de 10 noves hectàrees de vinya vella a tocar el seu celler de Gandesa. “Les vinyes velles, no les podem perdre. El pagès que es retira i deixa la terra no té cap interès que es perdin i si ho gestionem bé podem conservar el patrimoni i fer uns vins excel·lents.” De les tan sols 60 hectàrees de garnatxa peluda que hi ha a Catalunya, Edetària en treballa cinc. “És una varietat que té un toc una mica salvatge i s'ha d'amorosir.” Històricament la peluda es va deixar de banda perquè és de rendiment molt irregular.</p>
                <p>El celler produeix 160.000 ampolles l'any i n'exporta el 80%. Suïssa és el mercat on la firma troba més bona acollida i, tot seguit, el Japó i Suècia. El trident que conformen Alemanya, Holanda i Bèlgica també impulsa les exportacions d'Edetària. El celler també busca més penetració als Estats Units. L'empresa ha optat per no operar al mercat espanyol.</p>
                <p>És un vi de botiga o un vi de restaurant? “En aquest tema nosaltres decidim poc. Depèn de l'estratègia de cada distribuïdor”, diu Lliberia.</p>

            </article>
        </section>
        
        
        <section class="wrapper wrapper-margin">
            <div class="pagination">
                <div class="next-prev">
                    <a href="noticies-exemple-entrada.php" class="prev-post">
                        Wine Spectator top values 100: Via Terra negre 2017
                    </a>
                    <a href="noticies-exemple-entrada.php" class="next-post">
                        El Nacional.cat i Meritxell Falgueras seleccionen Edetària Selecció blanc nº1
                    </a>
                </div>
            </div>
        </section>
        
        
        <section class="wrapper wrapper-margin">
            <aside class="aside">
                <div class="widget widget-recent-posts">		
                    <h3 class="widget-title">Últimes entrades</h3>		
                    <ul>
                        <li>
                            <a href="noticies-exemple-entrada.php">Wine Spectator top values 100: Via Terra negre 2017</a>
                        </li>
                        <li>
                            <a href="noticies-exemple-entrada.php">El País Semanal & Carlos Delgado: La Terrenal 2015</a>
                        </li>
                        <li>
                            <a href="noticies-exemple-entrada.php">Wine Spectator Top 100: Via Terra Blanc 2017</a>
                        </li>
                        <li>
                            <a href="noticies-exemple-entrada.php">El Nacional.cat i Meritxell Falgueras seleccionen Edetària Selecció blanc nº1</a>
                        </li>
                        <li>
                            <a href="l-economic-del-punt-avui-amb-edetaria-i-la-do-terrra-alta.php">L'Econòmic del Punt Avui amb Edetària i la DO Terrra Alta</a>
                        </li>
                    </ul>
                </div>
                <div class="widget widget-archives">		
                    <h3 class="widget-title">Clipping</h3>		
                    <ul>
                        <li>
                            <a href="#">2016</a>
                        </li>
                        <li>
                            <a href="#">2017</a>
                        </li>
                        <li>
                            <a href="#">2018</a>
                        </li>
                        <li>
                            <a href="#">2019</a>
                        </li>
                    </ul>
                </div>

                <div class="widget widget-category">
                    <h3 class="widget-title">Categories</h3>		
                    <ul>
                        <li>
                            <a href="#">Grarnatxa</a>
                        </li>
                        <li>
                            <a href="#">DO Terra Alta</a>
                        </li>
                        <li>
                            <a href="#">Guies</a>
                        </li>
                        <li>
                            <a href="#">Cellers</a>
                        </li>
                        <li>
                            <a href="#">Gastronomia</a>
                        </li>
                        <li>
                            <a href="#">Premsa</a>
                        </li>
                        <li>
                            <a href="#">Premis</a>
                        </li>
                    </ul>
                </div>
                
                <div class="widget widget-search">
                    <h3 class="widget-title">Buscar notícies</h3>	
                    <!-- search -->
                    <form class="search" method="get" action="#" role="search">
                        <input class="search-input" type="search" name="s" placeholder="Buscar...">
                        <button class="search-submit" type="submit" role="button">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"></path></svg>
                        </button>
                    </form>
                    <!-- /search -->
                </div>
            </aside>
        </section>
        
        
        <section class="page-wrapper separator"></section>
        
    </main>


<?php include("footer.php"); ?>