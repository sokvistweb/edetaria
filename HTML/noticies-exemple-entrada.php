<?php
$pageTitle = 'L’Econòmic del Punt Avui amb Edetària';
$bodyClass = 'l-economic-del-punt-avui-amb-edetaria-i-la-do-terrra-alta';
include 'header.php';
?>

	
    <section class="billboard noheight">
        <div class="noslider">
            <div class="overlay"></div>
            <div class="single-img bg-img-noticies"></div>
        </div> <!-- /.noslider -->
    </section><!-- /.billboard  -->
    
    
    <main class="nomargin">
        
        <section class="separator-header"></section>

        <section class="intro wrapper wrapper-margin">
            
            <h1>Notícies</h1>
            
        </section><!--  End Features  -->
        
        
        <section class="wrapper wrapper-margin">
            <div class="spotlight">
                <div class="image">
                    <img src="assets/images/blog/img-06.jpg" alt="Edetària - Enoturisme" width="800" height="530" />
                </div>
                
                <div class="container">
                    <article class="content post">
                        <div class="entry-header">
                            <h1 class="entry-title">Això és el títol principal d'una entrada de prova</h1>
                            <div class="entry-meta single-post-meta">
                                <span class="post-category"><a href="#">Premsa, Premis</a></span>

                                <span class="post-date"><a href="#"><time class="entry-date" datetime="2012-11-09T23:15:57+00:00">4 de desembre de 2018</time></a></span>

                                <span class="post-author"><a href="#">Laura Pinto</a></span>
                            </div>
                        </div>
                    </article>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        
        <section class="wrapper wrapper-margin20">
            <article class="entry-content">

                <h2>Títol secundari H2</h2>

                <p>Aquest text és <b>bold</b> i aquest és <strong>strong</strong>. Aquest és <i>italic</i> i aquest és <em>emphasized</em>. Aquest és <u>underlined</u> i aquest és un <a class="a-reverse" href="#">link</a>.</p>

                <hr />
                <h2>Encapçalament nivell 2</h2>
                <h3>Encapçalament nivell 3</h3>
                <h4>Encapçalament nivell 4</h4>
                <hr />
                <p>Nunc lacinia ante nunc ac lobortis. Interdum adipiscing gravida odio porttitor sem non mi integer non faucibus ornare mi ut ante amet placerat aliquet. Volutpat eu sed ante lacinia sapien lorem accumsan varius montes viverra nibh in adipiscing blandit tempus accumsan.</p>

                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis dapibus rutrum facilisis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam tristique libero eu nibh porttitor fermentum. Nullam venenatis erat id vehicula viverra.</p>

                <a href="#" class="more-link">Inscriu-te aquí <span>→</span></a>

                <h2>Interdum sed dapibus</h2>
                <h3>Blockquote</h3>
                <blockquote>Lorem ipsum dolor vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent. Lorem ipsum dolor. Lorem ipsum dolor vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus.</blockquote>

                <h2>Llistes</h2>

                <h3>No ordenada</h3>
                <ul>
                    <li>Dolor etiam magna etiam.</li>
                    <li>Sagittis lorem eleifend.</li>
                    <li>Felis dolore viverra.</li>
                    <li>Sagittis lorem eleifend.</li>
                    <li>Felis feugiat viverra.</li>
                    <li>Etiam vel lorem sed viverra.</li>
                </ul>

                <h3>Ordenada</h3>
                <ol>
                    <li>Dolor etiam magna etiam.</li>
                    <li>Etiam vel lorem sed viverra.</li>
                    <li>Felis dolore viverra.</li>
                    <li>Dolor etiam magna etiam.</li>
                    <li>Etiam vel lorem sed viverra.</li>
                    <li>Felis dolore viverra.</li>
                </ol>
                
                <h2>Taula</h2>
                
                <table>
                    <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Descripció</th>
                            <th>Preu</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Item1</td>
                            <td>Ante turpis integer aliquet porttitor.</td>
                            <td>29.99</td>
                        </tr>
                        <tr>
                            <td>Item2</td>
                            <td>Vis ac commodo adipiscing arcu aliquet.</td>
                            <td>19.99</td>
                        </tr>
                        <tr>
                            <td>Item3</td>
                            <td> Morbi faucibus arcu accumsan lorem.</td>
                            <td>329.00</td>
                        </tr>
                        <tr>
                            <td>Item4</td>
                            <td>Vitae integer tempus condimentum.</td>
                            <td>9.99</td>
                        </tr>
                        <tr>
                            <td>Item5</td>
                            <td>Ante turpis integer aliquet porttitor.</td>
                            <td>229.99</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="2"></td>
                            <td>100.00</td>
                        </tr>
                    </tfoot>
                </table>

                <h2>Text amb imatges</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis dapibus rutrum facilisis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam tristique libero eu nibh porttitor fermentum.</p>

                <p><img class="alignleft" src="assets/images/blog/img-01.jpg" alt="" width="300" height="199" />Lorem ipsum dolor sit accumsan interdum nisi, quis tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent.</p>

                <p><img class="alignright" src="assets/images/blog/img-02.jpg" alt="" width="300" height="199" />Lorem ipsum dolor sit accumsan interdum nisi, quis tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent.</p>

                <p>Lorem ipsum dolor sit accumsan interdum nisi, quis tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus.</p>
                <img class="aligncenter" src="assets/images/blog/img-03.jpg" alt="" />
                
            </article>
        </section>
        
        
        <section class="wrapper wrapper-margin">
            <div class="pagination">
                <div class="next-prev">
                    <a href="noticies-exemple-entrada.php" class="prev-post">
                        Wine Spectator top values 100: Via Terra negre 2017
                    </a>
                    <a href="l-economic-del-punt-avui-amb-edetaria-i-la-do-terrra-alta.php" class="next-post">
                        L'Econòmic del Punt Avui amb Edetària i la DO Terrra Alta
                    </a>
                </div>
            </div>
        </section>
        
        
        <section class="wrapper wrapper-margin">
            <aside class="aside">
                <div class="widget widget-recent-posts">		
                    <h3 class="widget-title">Últimes entrades</h3>		
                    <ul>
                        <li>
                            <a href="noticies-exemple-entrada.php">Wine Spectator top values 100: Via Terra negre 2017</a>
                        </li>
                        <li>
                            <a href="noticies-exemple-entrada.php">El País Semanal & Carlos Delgado: La Terrenal 2015</a>
                        </li>
                        <li>
                            <a href="noticies-exemple-entrada.php">Wine Spectator Top 100: Via Terra Blanc 2017</a>
                        </li>
                        <li>
                            <a href="noticies-exemple-entrada.php">El Nacional.cat i Meritxell Falgueras seleccionen Edetària Selecció blanc nº1</a>
                        </li>
                        <li>
                            <a href="l-economic-del-punt-avui-amb-edetaria-i-la-do-terrra-alta.php">L'Econòmic del Punt Avui amb Edetària i la DO Terrra Alta</a>
                        </li>
                    </ul>
                </div>
                <div class="widget widget-archives">		
                    <h3 class="widget-title">Clipping</h3>		
                    <ul>
                        <li>
                            <a href="#">2016</a>
                        </li>
                        <li>
                            <a href="#">2017</a>
                        </li>
                        <li>
                            <a href="#">2018</a>
                        </li>
                        <li>
                            <a href="#">2019</a>
                        </li>
                    </ul>
                </div>

                <div class="widget widget-category">
                    <h3 class="widget-title">Categories</h3>		
                    <ul>
                        <li>
                            <a href="#">Grarnatxa</a>
                        </li>
                        <li>
                            <a href="#">DO Terra Alta</a>
                        </li>
                        <li>
                            <a href="#">Guies</a>
                        </li>
                        <li>
                            <a href="#">Cellers</a>
                        </li>
                        <li>
                            <a href="#">Gastronomia</a>
                        </li>
                        <li>
                            <a href="#">Premsa</a>
                        </li>
                        <li>
                            <a href="#">Premis</a>
                        </li>
                    </ul>
                </div>
                
                <div class="widget widget-search">
                    <h3 class="widget-title">Buscar notícies</h3>	
                    <!-- search -->
                    <form class="search" method="get" action="#" role="search">
                        <input class="search-input" type="search" name="s" placeholder="Buscar...">
                        <button class="search-submit" type="submit" role="button">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"></path></svg>
                        </button>
                    </form>
                    <!-- /search -->
                </div>
            </aside>
        </section>
        
        
        <section class="page-wrapper separator"></section>
        
    </main>


<?php include("footer.php"); ?>