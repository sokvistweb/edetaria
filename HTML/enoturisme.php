<?php
$pageTitle = 'Experiències: Enoturisme';
$bodyClass = 'enoturisme';
include 'header.php';
?>

	
    <section class="billboard halfheight">
        <div class="noslider">
            <div class="overlay"></div>
            <div class="single-img bg-img-enoturisme"></div>
        </div> <!-- /.noslider -->
    </section><!-- /.billboard  -->
    
    
    <main class="halfmargin">
        
        <section class="separator-header"></section>

        <section class="intro wrapper wrapper-margin">
            
            <h1>Enoturisme</h1>
            
        </section><!--  End Features  -->


        <section class="page-wrapper" id="passeig-entre-vinyes">
            <div class="spotlight spotlight-full">
                <div class="container container-full">
                    <div class="content">
                        <h2>Passeig entre vinyes</h2>
                        <p>T’expliquem la nostra historia tot passejant entre vinyes, i experimenta sensacions tastant 4 vins.</p>
                    </div>
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight">
                <div class="image">
                    <img src="assets/images/page-enoturisme-passeig_entre_vinyes.jpg" alt="Edetària - Enoturisme" width="900" height="520" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <dl>
                            <dt>Mínim persones:</dt>
                            <dd>2</dd>
                            
                            <dt>Preu:</dt>
                            <dd>18€/pax.</dd>
                            
                            <dt>Durada:</dt>
                            <dd>1,5 h.</dd>
                            
                            <dt>Període:</dt>
                            <dd>Tot l’any.</dd>
                            
                            <dt>Coneixement del vi:</dt>
                            <dd>Principiant /Aficionat/ Winelover /Professional</dd>
                        </dl>
                        <a href="mailto:visitesceller@edetaria.com" title="Reserva aquesta activitat" class="more-link" target="_blank">Reserva aquí</a>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="separator-middle" id="d-on-ve-edetaria"></section>
        
        <section class="page-wrapper" id="experiencia-gourmet">
            <div class="spotlight spotlight-full">
                <div class="container container-full">
                    <div class="content">
                        <h2>Experiència gourmet</h2>
                        <p>En el mas situat a la finca del celler envoltat per vinyes, tenim a la teva disposició un espai exclusiu per a posar a prova els sentits. Tastaràs els nostres vins més top maridats amb productes autòctons.</p>
                    </div>
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight">
                <div class="image">
                    <img src="assets/images/page-enoturisme-experiencia_gourmet.jpg" alt="Edetària - Enoturisme" width="900" height="520" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <dl>
                            <dt>Mínim persones:</dt>
                            <dd>4</dd>
                            
                            <dt>Preu:</dt>
                            <dd>35€/pax.</dd>
                            
                            <dt>Durada:</dt>
                            <dd>2 h.</dd>
                            
                            <dt>Període:</dt>
                            <dd>Tot l’any.</dd>
                            
                            <dt>Coneixement del vi:</dt>
                            <dd>Principiant /Aficionat/ Winelover /Professional</dd>
                        </dl>
                        <a href="mailto:visitesceller@edetaria.com" title="Reserva aquesta activitat" class="more-link" target="_blank">Reserva aquí</a>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="separator-middle" id="d-on-ve-edetaria"></section>
        
        <section class="page-wrapper" id="els-top">
            <div class="spotlight spotlight-full">
                <div class="container container-full">
                    <div class="content">
                        <h2>Els nostres TOP's</h2>
                        <p>Perquè el vi és per tu un estil de vida.</p>
                        <p>Tastaràs els vins Edetària blanc i negre i els nostres quatre vins de Finca (La Terrenal, La Pedrissa, La Genuïna i La Personal).</p>
                    </div>
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight">
                <div class="image">
                    <img src="assets/images/page-enoturisme-els_top.jpg" alt="Edetària - Enoturisme" width="900" height="520" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <dl>
                            <dt>Mínim persones:</dt>
                            <dd>6</dd>
                            
                            <dt>Preu:</dt>
                            <dd>30€/pax.</dd>
                            
                            <dt>Durada:</dt>
                            <dd>1,5 h.</dd>
                            
                            <dt>Període:</dt>
                            <dd>Tot l’any.</dd>
                            
                            <dt>Coneixement del vi:</dt>
                            <dd>Principiant /Aficionat/ Winelover /Professional</dd>
                        </dl>
                        <a href="mailto:visitesceller@edetaria.com" title="Reserva aquesta activitat" class="more-link" target="_blank">Reserva aquí</a>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="separator-middle" id="d-on-ve-edetaria"></section>
        
        <section class="page-wrapper" id="creativa">
            <div class="spotlight spotlight-full">
                <div class="container container-full">
                    <div class="content">
                        <h2>Creativa</h2>
                        <p>Vols ser enòleg per un dia?</p>
                        <p>Et proporcionem tot allò que necessites per a crear el teu propi vi!</p>
                        <p>Et donem un kit de creació amb 2 provetes, 6 copes, 3 varietats de vi i fitxa de cada varietat.</p>
                    </div>
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight">
                <div class="image">
                    <img src="assets/images/page-enoturisme-creativa.jpg" alt="Edetària - Enoturisme" width="900" height="520" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <dl>
                            <dt>Mínim persones:</dt>
                            <dd>2</dd>
                            
                            <dt>Preu:</dt>
                            <dd>20€/pax.</dd>
                            
                            <dt>Durada:</dt>
                            <dd>2 h.</dd>
                            
                            <dt>Període:</dt>
                            <dd>Tot l’any.</dd>
                            
                            <dt>Coneixement del vi:</dt>
                            <dd>Principiant /Aficionat/ Winelover /Professional</dd>
                        </dl>
                        <a href="mailto:visitesceller@edetaria.com" title="Reserva aquesta activitat" class="more-link" target="_blank">Reserva aquí</a>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="separator-middle" id="d-on-ve-edetaria"></section>
        
        <section class="page-wrapper" id="nens">
            <div class="spotlight spotlight-full">
                <div class="container container-full">
                    <div class="content">
                        <h2>Nens</h2>
                        <p>Ens ocupem de que els nens gaudeixin d’una experiència molt divertida amb un monitor, mentre tu fas la teva visita d’adults.</p>
                        <p>Quina els agradarà més?</p>
                    </div>
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight">
                <div class="image">
                    <img src="assets/images/page-enoturisme-nens-1.jpg" alt="Edetària - Enoturisme" width="900" height="520" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <h3>Les plantes i el vi</h3>
                        <p>Un taller on parlarem de les plantes de la mediterrània i els ensenyarem a trobar els seus aromes en els vins.</p>
                        <dl>
                            <dt>Edat recomanada:</dt>
                            <dd>3 / 13 anys</dd>
                            
                            <dt>Mínim:</dt>
                            <dd>2 nens</dd>
                            
                            <dt>Preu:</dt>
                            <dd>8€/pax.</dd>
                            
                            <dt>Durada:</dt>
                            <dd>1,5 h.</dd>
                            
                            <dt>Període:</dt>
                            <dd>Tot l’any.</dd>
                        </dl>
                        <a href="mailto:visitesceller@edetaria.com" title="Reserva aquesta activitat" class="more-link" target="_blank">Reserva aquí</a>
                    </div>
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight">
                <div class="image">
                    <img src="assets/images/page-enoturisme-nens-2.jpg" alt="Edetària - Enoturisme" width="900" height="520" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <h3>Dissenya l’etiqueta d’un vi</h3>
                        <p>Explicarem tot el que fem en un celler i, a més, farem un taller de disseny d’etiquetes d’ampolles. Se l'enduran a casa!</p>
                        <dl>
                            <dt>Edat recomanada:</dt>
                            <dd>3 / 13 anys</dd>
                            
                            <dt>Mínim:</dt>
                            <dd>2 nens</dd>
                            
                            <dt>Preu:</dt>
                            <dd>8€/pax.</dd>
                            
                            <dt>Durada:</dt>
                            <dd>1,5 h.</dd>
                            
                            <dt>Període:</dt>
                            <dd>Tot l’any.</dd>
                        </dl>
                        <a href="mailto:visitesceller@edetaria.com" title="Reserva aquesta activitat" class="more-link" target="_blank">Reserva aquí</a>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="separator-middle" id="d-on-ve-edetaria"></section>
        
        <section class="page-wrapper" id="escoltant-les-vinyes">
            <div class="spotlight spotlight-full">
                <div class="container container-full">
                    <div class="content">
                        <h2>Escoltant les vinyes</h2>
                        <p>Vols viure una experiència única amb la teva parella? Vine a perdre’t entre vinyes que et parlen a cau d’orella.</p>
                    </div>
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight">
                <div class="image">
                    <img src="assets/images/page-enoturisme-escoltant_les_vinyes.jpg" alt="Edetària - Enoturisme" width="900" height="520" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <p>L’activitat inclou: cistell de pagès amb 2 ampolles de vi, embotits autòctons i plànol de la finca.</p>
                        <dl>
                            <dt>Mínim persones:</dt>
                            <dd>2</dd>
                            
                            <dt>Preu:</dt>
                            <dd>50€/parella</dd>
                            
                            <dt>Període:</dt>
                            <dd>D'abril a octubre</dd>
                        </dl>
                        <a href="mailto:visitesceller@edetaria.com" title="Reserva aquesta activitat" class="more-link" target="_blank">Reserva aquí</a>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="separator-middle" id="d-on-ve-edetaria"></section>
        
        <section class="page-wrapper" id="crea-la-teva-experiencia">
            <div class="spotlight spotlight-full">
                <div class="container container-full">
                    <div class="content">
                        <h2>Crea la teva experiència</h2>
                        <p>Tens pensada una experiència diferent a les que et proposem?</p>
                        <p>Escoltem la teva proposta i intentarem fer-la realitat. Explica’ns.</p>
                    </div>
                </div>
            </div><!-- /.spotlight -->
            
            <div class="spotlight">
                <div class="image">
                    <img src="assets/images/page-enoturisme-crea_la_teva.jpg" alt="Edetària - Enoturisme" width="900" height="520" />
                </div>
                
                <div class="container">
                    <div class="content">
                        <dl>
                            <dt>Mínim persones:</dt>
                            <dd>4</dd>
                            
                            <dt>Preu:</dt>
                            <dd>A convenir</dd>
                            
                            <dt>Durada:</dt>
                            <dd>2 h.</dd>
                            
                            <dt>Període:</dt>
                            <dd>Tot l’any.</dd>
                            
                            <dt>Coneixement del vi:</dt>
                            <dd>Principiant /Aficionat/ Winelover /Professional</dd>
                        </dl>
                        <a href="mailto:visitesceller@edetaria.com" title="Reserva aquesta activitat" class="more-link" target="_blank">Reserva aquí</a>
                    </div>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        <section class="page-wrapper separator"></section>
        
    </main>


<?php include("footer.php"); ?>