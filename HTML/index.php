<?php
$pageTitle = 'L’esperit d’una terra';
$bodyClass = 'inici';
include 'header.php';
?>


	<section class="billboard fullheight">
        <div class="title-block">
            <div class="title-bg">
                <img class="main-logo" src="assets/images/edetaria-logo-tot.svg" alt="Edetària logo" width="500" height="500" />
                <h1 class="big-title">vins autèntics</h1>
                <h2 class="title-desc">L'esperit d'una terra</h2>
            </div>
        </div>
        <div class="slider">
            <div class="flexslider">
                <ul class="slides">
                    <li>
                        <div class="overlay"></div>
                        <div class="slider-caption visible-md visible-lg"></div>
                    </li>
                    <li>
                        <div class="overlay"></div>
                        <div class="slider-caption visible-md visible-lg"></div>
                    </li>
                </ul>
            </div> <!-- /.flexslider -->
        </div> <!-- /.slider -->
    </section><!-- /.billboard  -->
    
    <main class="fullmargin">
    
        <section class="page-wrapper">
            <div class="spotlight grid">
                
                <figure class="effect-skv image">
                    <img src="assets/images/som-01.jpg" alt="Edetària" width="900" height="520" />
                    <figcaption>
                        <div class="fig-content">
                            <h3>Edetària</h3>
                            <h2>Un estil propi</h2>
                            <div class="fig-subcontent">
                                <p class="to-anchor"><a href="edetaria.php#qui-som">Qui som</a></p>
                                <p class="to-anchor"><a href="edetaria.php#un-estil-propi">Un estil propi</a></p>
                                <p class="to-anchor"><a href="edetaria.php#territori">Territori</a></p>
                                <p class="to-anchor"><a href="edetaria.php#d-on-ve-edetaria">D'on ve Edetària</a></p>
                                <p class="to-anchor"><a href="edetaria.php#equip">L'equip</a></p>
                                <div class="separator-hover2"></div>
                            </div>
                        </div>
                        <a href="edetaria.php">Veure més<span class="fa fa-shopping-cart"></span></a>
                    </figcaption>			
                </figure>
                
                <figure class="effect-skv2 color1 image">
                    <img src="assets/images/vinyes-i-terra-01.jpg" alt="Edetària" width="900" height="520" />
                    <figcaption>
                        <div class="fig-content">
							<h2>Vinyes i terra</h2>
							<p>La vinya: el nostre valor més important</p>
                            <div class="separator-hover6"></div>
                        </div>
				        <a href="vinyes-i-terra.php">Veure més<span class="fa fa-arrow-right"></span></a>
                    </figcaption>
                </figure>
            </div><!-- /.spotlight -->
        </section>
            
            
        <section class="page-wrapper">
            <div class="spotlight grid">
                <div class="container-full">
                    <figure class="effect-skv2 image">
                        <img class="large-img" src="assets/images/els-vins.jpg" alt="Edetària" width="1800" height="460" />
                        <img class="small-img" src="assets/images/els-vins-s.jpg" alt="Edetària" width="900" height="520" />
                        <figcaption>
                            <div class="fig-content">
                                <h2>Els vins</h2>
                                <p>Vins amb caràcter i expressió</p>
                            </div>
                            <a href="els-vins.php">Comprar online</a>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </section>
          
            
        <section class="page-wrapper">
            <div class="spotlight grid">
                <div class="container33">
                    <figure class="effect-skv3 image">
                        <img class="large-img" src="assets/images/experiencies-enoturisme.jpg" alt="Edetària" width="900" height="460" />
                        <img class="small-img" src="assets/images/experiencies-enoturisme-2.jpg" alt="Edetària" width="900" height="520" />
                        <figcaption>
                            <div class="fig-content">
                                <h2>Enoturisme</h2>
                                <ul>
                                    <li><a href="enoturisme.php#passeig-entre-vinyes">Passeig entre vinyes</a></li>
                                    <li><a href="enoturisme.php#experiencia-gourmet">Experiència Gourmet</a></li>
                                    <li><a href="enoturisme.php#els-top">Els nostres TOP's</a></li>
                                    <li><a href="enoturisme.php#creativa">Creativa</a></li>
                                    <li><a href="enoturisme.php#nens">Nens</a></li>
                                    <li><a href="enoturisme.php#escoltant-les-vinyes">Escoltant les vinyes</a></li>
                                    <li><a href="enoturisme.php#crea-la-teva-experiencia">Crea la teva experiència</a></li>
                                </ul>
                            </div>
                            <a href="enoturisme.php">Veure més</a>
                        </figcaption>
                    </figure>
                </div>
                
                <div class="container33">
                    <figure class="effect-skv3 image">
                        <img class="large-img" src="assets/images/experiencies-empreses.jpg" alt="Edetària" width="900" height="460" />
                        <img class="small-img" src="assets/images/experiencies-empreses-2.jpg" alt="Edetària" width="900" height="520" />
                        <figcaption>
                            <div class="fig-content">
                                <h2>Empreses</h2>
                                <p><em>Deixa de planificar reunions, comença a dissenyar experiències</em></p>
                            </div>
                            <a href="empreses.php">Veure més</a>
                        </figcaption>
                    </figure>
                </div>
                
                <div class="container33">
                    <figure class="effect-skv3 image">
                        <img class="large-img" src="assets/images/experiencies-espais.jpg" alt="Edetària" width="900" height="460" />
                        <img class="small-img" src="assets/images/experiencies-espais-2.jpg" alt="Edetària" width="900" height="520" />
                        <figcaption>
                            <div class="fig-content">
                                <h2>Espais</h2>
                                <p><em>La teva inquietud, la fem realitat</em></p>
                            </div>
                            <a href="espais.php">Veure més</a>
                        </figcaption>
                    </figure>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        
        
        <section class="page-wrapper">
            <div class="spotlight grid">
                
                <figure class="effect-skv color1 image">
                    <img src="assets/images/el-celler.jpg" alt="Edetària" width="900" height="520" />
                    <figcaption>
                        <div class="fig-content">
                            <h3>El celler</h3>
                            <h2>Quan estimes un territori hi vius amb sintonia</h2>
                            <div class="fig-subcontent">
                                <p class="to-anchor"><a href="el-celler.php#criansa">Criança</a></p>
                                <p class="no-bg"><em>Els vins d’Edetària, l’expressió d’una personalitat</em></p>
                                <p class="to-anchor"><a href="el-celler.php#la-verema">La verema</a></p>
                                <p class="no-bg"><em>Només els millors gotims</em></p>
                                <p class="to-anchor"><a href="el-celler.php#vinificacions">Vinificacions</a></p>
                                <p class="no-bg"><em>L’expressió de cada parcel•la</em></p>
                            </div>
                        </div>
                        <a href="el-celler.php">Veure més</a>
                    </figcaption>			
                </figure>
                
                <figure class="effect-skv image">
                    <img src="assets/images/noticies.jpg" alt="Edetària" width="900" height="520" />
                    <figcaption>
                        <div class="fig-content">
							<h3>Notícies</h3>
							<h2></h2>
							<p><a href="#">El País Semanal & Carlos Delgado: La Terrenal 2015</a></p>
							<p><a href="#">Wine Spectator Top 100: Via Terra Blanc 2017</a></p>
							<p><a href="#">El Nacional.cat i Meritxell Falgueras seleccionen Edetària Selecció blanc nº1</a></p>
							<p><a href="#">Guía Peñín 2019: Via Edetana 91p. La Terrenal 94p. Edetària Selecció Negre 94 p.</a></p>
                        </div>
				        <a href="noticies.php">Totes les notícies</a>
                    </figcaption>
                </figure>
            </div><!-- /.spotlight -->
        </section>
        
        
        
        <section class="page-wrapper separator">
            
        </section>
        
    </main>


<?php include("footer.php"); ?>