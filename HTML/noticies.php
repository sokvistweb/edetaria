<?php
$pageTitle = 'Notícies';
$bodyClass = 'noticies';
include 'header.php';
?>

	
    <section class="billboard noheight">
        <div class="noslider">
            <div class="overlay"></div>
            <div class="single-img bg-img-noticies"></div>
        </div> <!-- /.noslider -->
    </section><!-- /.billboard  -->
    
    
    <main class="nomargin">
        
        <section class="separator-header"></section>

        <section class="intro wrapper wrapper-margin">
            
            <h1>Notícies</h1>
            
        </section><!--  End Features  -->


        <section class="wrapper wrapper-margin20">
            <div class="spotlight">
                <div class="image entry-image">
                    <img src="assets/images/blog/img-01.jpg" alt="Edetària - Enoturisme" width="800" height="530" />
                </div>
                
                <div class="container entry-container">
                    <article class="content post">
                        <div class="entry-header">
                            <h2>
                                <a href="noticies-exemple-entrada.php">Wine Spectator top values 100: Via Terra negre 2017</a>
                            </h2>
                            <div class="entry-meta">
                                <span class="post-category"><a href="#">Garnatxa</a></span>

                                <span class="post-date"><a href="#"><time class="entry-date" datetime="2012-11-09T23:15:57+00:00">20 de desembre de 2018</time></a></span>

                                <span class="post-author"><a href="#">Laura Pinto</a></span>

                                <span class="comments-link"><a href="#">2 Comentaris</a></span>
                            </div>
                        </div>
                        <div class="entry-excerpt clearfix">
                            <p>Wine Spectator inclou Via Terra Negre 2017 a la llista Top Values 2018 Elegant Reds ...</p>
                            <div class="read-more cl-effect-14">
                                <a href="noticies-exemple-entrada.php" class="more-link">Seguir llegint <span class="meta-nav">→</span></a>
                            </div>
                        </div>
                    </article>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        
        <section class="wrapper wrapper-margin20">
            <div class="spotlight">
                <div class="image entry-image">
                    <img src="assets/images/blog/img-02.jpg" alt="Edetària - Enoturisme" width="800" height="530" />
                </div>
                
                <div class="container entry-container">
                    <article class="content post">
                        <div class="entry-header">
                            <h2>
                                <a href="noticies-exemple-entrada.php">El País Semanal & Carlos Delgado: La Terrenal 2015</a>
                            </h2>
                            <div class="entry-meta">
                                <span class="post-category"><a href="#">Premsa</a></span>

                                <span class="post-date"><a href="#"><time class="entry-date" datetime="2012-11-09T23:15:57+00:00">10 de desembre de 2018</time></a></span>

                                <span class="post-author"><a href="#">Laura Pinto</a></span>

                                <span class="comments-link"><a href="#">4 Comentaris</a></span>
                            </div>
                        </div>
                        <div class="entry-excerpt clearfix">
                            <p>El País Semanal i Carlos Delgado seleccionen Finca La Terrenal 2015 a la llista de vins Gourmand de Nadal.</p>
                            <a href="noticies-exemple-entrada.php" class="more-link">Seguir llegint <span class="meta-nav">→</span></a>
                        </div>
                    </article>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        
        <section class="wrapper wrapper-margin20">
            <div class="spotlight">
                <div class="image entry-image">
                    <img src="assets/images/blog/img-03.jpg" alt="Edetària - Enoturisme" width="800" height="530" />
                </div>
                
                <div class="container entry-container">
                    <article class="content post">
                        <div class="entry-header">
                            <h2>
                                <a href="noticies-exemple-entrada.php">Wine Spectator Top 100: Via Terra Blanc 2017</a>
                            </h2>
                            <div class="entry-meta">
                                <span class="post-category"><a href="#">Premsa</a></span>

                                <span class="post-date"><a href="#"><time class="entry-date" datetime="2012-11-09T23:15:57+00:00">5 de desembre de 2018</time></a></span>

                                <span class="post-author"><a href="#">Laura Pinto</a></span>

                                <span class="comments-link"><a href="#">3 Comentaris</a></span>
                            </div>
                        </div>
                        <div class="entry-excerpt clearfix">
                            <p>La garnatxa blanca 100% DO Terra Alta nº47 al Top 100 de Wine Spectator amb Edetària Via Terra Blanc 2017.</p>
                            <a href="noticies-exemple-entrada.php" class="more-link">Seguir llegint <span class="meta-nav">→</span></a>
                        </div>
                    </article>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        
        <section class="wrapper wrapper-margin20">
            <div class="spotlight">
                <div class="image entry-image">
                    <img src="assets/images/blog/img-04.jpg" alt="Edetària - Enoturisme" width="800" height="530" />
                </div>
                
                <div class="container entry-container">
                    <article class="content post">
                        <div class="entry-header">
                            <h2>
                                <a href="noticies-exemple-entrada.php">El Nacional.cat i Meritxell Falgueras seleccionen Edetària Selecció blanc nº1</a>
                            </h2>
                            <div class="entry-meta">
                                <span class="post-category"><a href="#">Garnatxa</a></span>

                                <span class="post-date"><a href="#"><time class="entry-date" datetime="2012-11-09T23:15:57+00:00">24 de novembre de 2018</time></a></span>

                                <span class="post-author"><a href="#">Laura Pinto</a></span>

                                <span class="comments-link"><a href="#">2 Comentaris</a></span>
                            </div>
                        </div>
                        <div class="entry-excerpt clearfix">
                            <p>Meritxell Falgueras i ElNacional.cat escullen Edetària Selecció blanc com a nº1 d'aquesta selecció. La garnatxa blanca DO Terra Alta protagonista ...</p>
                            <div class="read-more cl-effect-14">
                                <a href="noticies-exemple-entrada.php" class="more-link">Seguir llegint <span class="meta-nav">→</span></a>
                            </div>
                        </div>
                    </article>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        
        <section class="wrapper wrapper-margin20">
            <div class="spotlight">
                <div class="image entry-image">
                    <img src="assets/images/blog/img-05.jpg" alt="Edetària - Enoturisme" width="800" height="530" />
                </div>
                
                <div class="container entry-container">
                    <article class="content post">
                        <div class="entry-header">
                            <h2>
                                <a href="l-economic-del-punt-avui-amb-edetaria-i-la-do-terrra-alta.php">L'Econòmic del Punt Avui amb Edetària i la DO Terrra Alta</a>
                            </h2>
                            <div class="entry-meta">
                                <span class="post-category"><a href="#">Premsa, Premis</a></span>

                                <span class="post-date"><a href="#"><time class="entry-date" datetime="2012-11-09T23:15:57+00:00">2 de setembre de 2018</time></a></span>

                                <span class="post-author"><a href="#">Laura Pinto</a></span>
                            </div>
                        </div>
                        <div class="entry-excerpt clearfix">
                            <p>Entrevista a Joan Àngel Lliberia al semanari L'Econòmic del Punta Avui. Llegeix tota la informació. Premi pel cep més personal d'Edetària ...</p>
                            <div class="read-more cl-effect-14">
                                <a href="l-economic-del-punt-avui-amb-edetaria-i-la-do-terrra-alta.php" class="more-link">Seguir llegint <span class="meta-nav">→</span></a>
                            </div>
                        </div>
                    </article>
                </div>
            </div><!-- /.spotlight -->
        </section>
        
        
        <section class="wrapper wrapper-margin">
            <div class="pagination">
                <ul class="">
                    <li><span class="disabled">Ant.</span></li>
                    <li><a href="#" class="active">1</a></li>
                    <li><a href="#" class="">2</a></li>
                    <li><a href="#" class="">3</a></li>
                    <li><span>&hellip;</span></li>
                    <li><a href="#" class="">8</a></li>
                    <li><a href="#" class="">9</a></li>
                    <li><a href="#" class="">10</a></li>
                    <li><a href="#" class="">Seg.</a></li>
                </ul>
            </div>
        </section>
        
        
        <section class="wrapper wrapper-margin">
            <aside class="aside">
                <div class="widget widget-recent-posts">		
                    <h3 class="widget-title">Últimes entrades</h3>		
                    <ul>
                        <li>
                            <a href="noticies-exemple-entrada.php">Wine Spectator top values 100: Via Terra negre 2017</a>
                        </li>
                        <li>
                            <a href="noticies-exemple-entrada.php">El País Semanal & Carlos Delgado: La Terrenal 2015</a>
                        </li>
                        <li>
                            <a href="noticies-exemple-entrada.php">Wine Spectator Top 100: Via Terra Blanc 2017</a>
                        </li>
                        <li>
                            <a href="noticies-exemple-entrada.php">El Nacional.cat i Meritxell Falgueras seleccionen Edetària Selecció blanc nº1</a>
                        </li>
                        <li>
                            <a href="l-economic-del-punt-avui-amb-edetaria-i-la-do-terrra-alta.php">L'Econòmic del Punt Avui amb Edetària i la DO Terrra Alta</a>
                        </li>
                    </ul>
                </div>
                <div class="widget widget-archives">		
                    <h3 class="widget-title">Clipping</h3>		
                    <ul>
                        <li>
                            <a href="#">2016</a>
                        </li>
                        <li>
                            <a href="#">2017</a>
                        </li>
                        <li>
                            <a href="#">2018</a>
                        </li>
                        <li>
                            <a href="#">2019</a>
                        </li>
                    </ul>
                </div>

                <div class="widget widget-category">
                    <h3 class="widget-title">Categories</h3>		
                    <ul>
                        <li>
                            <a href="#">Grarnatxa</a>
                        </li>
                        <li>
                            <a href="#">DO Terra Alta</a>
                        </li>
                        <li>
                            <a href="#">Guies</a>
                        </li>
                        <li>
                            <a href="#">Cellers</a>
                        </li>
                        <li>
                            <a href="#">Gastronomia</a>
                        </li>
                        <li>
                            <a href="#">Premsa</a>
                        </li>
                        <li>
                            <a href="#">Premis</a>
                        </li>
                    </ul>
                </div>
                
                <div class="widget widget-search">
                    <h3 class="widget-title">Buscar notícies</h3>	
                    <!-- search -->
                    <form class="search" method="get" action="#" role="search">
                        <input class="search-input" type="search" name="s" placeholder="Buscar...">
                        <button class="search-submit" type="submit" role="button">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"></path></svg>
                        </button>
                    </form>
                    <!-- /search -->
                </div>
            </aside>
        </section>
        
        
        <section class="page-wrapper separator"></section>
        
    </main>


<?php include("footer.php"); ?>